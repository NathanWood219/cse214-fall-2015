/**
* The <code>AuctionTable</code> class uses a hash table to store the information for multiple Auctions.
* 
* @author Nathan Wood
*    e-mail: nathan.wood@stonybrook.edu
*    Stony Brook ID: 109876625
 * <dt><b>Assignment:</b><dd>
 *    Homework #6 for CSE 214, Fall 2015
 * <dt><b>Date:</b><dd>
 *    November 17th, 2015
 **/

import java.util.Hashtable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import big.data.*;

public class AuctionTable implements Serializable {
	private static final long serialVersionUID = 1L;
	
	// ATTRIBUTES
	private Hashtable<String, Auction> table = new Hashtable<String, Auction>();
	
	// METHODS
	
	/**
	 * Attempts to create and return a new AuctionTable by retrieving the information from an input URL.
	 * 
	 * @param URL
	 * 	The link from which to fetch the data
	 * 
	 * @throws IllegalArgumentException
	 * 	Thrown if the URL was invalid or the file could not be found
	 * 
	 * @return
	 * 	Returns a newly constructed AuctionTable
	 */
	public static AuctionTable buildFromURL(String URL) throws IllegalArgumentException {
		try {
			DataSource ds = DataSource.connect(URL).load();
			
			String[] checkLength = ds.fetchStringArray("listing/auction_info/time_left");
			
			String[][] myList = new String[8][checkLength.length];
			int j = 0;
			
			myList[j++] = checkLength;
			myList[j++] = ds.fetchStringArray("listing/auction_info/current_bid");
			myList[j++] = ds.fetchStringArray("listing/auction_info/id_num");
			myList[j++] = ds.fetchStringArray("listing/seller_info/seller_name");
			myList[j++] = ds.fetchStringArray("listing/auction_info/high_bidder/bidder_name");
			myList[j++] = ds.fetchStringArray("listing/item_info/cpu");
			myList[j++] = ds.fetchStringArray("listing/item_info/memory");
			myList[j]   = ds.fetchStringArray("listing/item_info/hard_drive");
			
			AuctionTable auctionTable = new AuctionTable();
			
			for(int i=0; i<myList[0].length; i++) {
				String timeLeft = myList[0][i];
				int hours = 0;
				
				if(timeLeft.contains("days")) {
					hours += 24 * Integer.parseInt(timeLeft.substring(0, timeLeft.indexOf("days")-1));
					if(timeLeft.contains("hours")) {
						hours += Integer.parseInt(timeLeft.substring(timeLeft.indexOf("days") + 6, timeLeft.indexOf("hours") - 1));
					}
				} else if(timeLeft.contains("hours")){
					hours += Integer.parseInt(timeLeft.substring(0, timeLeft.indexOf("hours") - 1));
				}
				
				double currentBid = Double.parseDouble(myList[1][i].substring(1).replace(",", ""));
				String itemInfo = myList[5][i] + " - " + myList[6][i] + " - " + myList[7][i];
				itemInfo = itemInfo.replace(" -  - ", "");
				
				auctionTable.putAuction(myList[2][i], new Auction(hours, currentBid, myList[2][i],
						myList[3][i].replace("\n", "").replace(" ", ""), myList[4][i], itemInfo));
			}
			
			return auctionTable;
			
		} catch(Exception e) {
			throw new IllegalArgumentException();
		}
	}
	
	/**
	 * Adds a new Auction to this AuctionTable using the ID as a key for the table
	 * 
	 * @param auctionID
	 * 	The ID of the Auction to be used as the key
	 * 
	 * @param auction
	 * 	The Auction that is to be added to the table
	 * 
	 * @throws IllegalArgumentException
	 * 	Thrown if the AuctionTable already contains the input ID
	 * 
	 * <dt><b>Postcondition:</b><dd>
	 *  The AuctionTable will now contain the input Auction
	 */
	public void putAuction(String auctionID, Auction auction) throws IllegalArgumentException {
		// Throw an illegal argument exception if the auctionID already exists in this table
		if(table.containsKey(auctionID)) {
			throw new IllegalArgumentException();
		}
		
		table.put(auctionID, auction);
	}
	
	/**
	 * Retrieves the Auction with the input ID from the table
	 * 
	 * @param auctionID
	 * 	The ID of the Auction that is to be retrieved
	 * 
	 * @return
	 * 	The Auction associated with the input ID
	 */
	public Auction getAuction(String auctionID) {
		return table.get(auctionID);
	}
	
	/**
	 * Manually passes the input amount of time for every Auction in the table
	 * 
	 * @param numHours
	 * 	The number of hours to have pass
	 * 
	 * @throws IllegalArgumentException
	 * 	Thrown if the number of hours is negative
	 * 
	 * <dt><b>Postcondition:</b><dd>
	 *  All of the Auction objects in the table will have the set time pass
	 */
	public void letTimePass(int numHours) throws IllegalArgumentException {
		if(numHours < 0) {
			throw new IllegalArgumentException();
		}
		
		Enumeration<Auction> tempList = table.elements();
		
		while(tempList.hasMoreElements()) {
			tempList.nextElement().decrementTimeRemaining(numHours);
		}
	}

	/**
	 * Searches for and removes all Auction objects that have no more time remaining
	 * 
	 * <dt><b>Postcondition:</b><dd>
	 *  All of the Auction objects at 0 time remaining will have been removed
	 */
	public void removeExpiredAuctions() {
		Enumeration<Auction> tempList = table.elements();
		Auction tempAuction;
		
		while(tempList.hasMoreElements()) {
			tempAuction = tempList.nextElement();
			
			if(tempAuction.getTimeRemaining() == 0) {
				table.remove(tempAuction.getAuctionID());
			}
		}
	}

	/**
	 * Prints a formatted version of the table
	 * 
	 * <dt><b>Postcondition:</b><dd>
	 *  A formatted table of all the Auction objects will have been printed
	 */
	public void printTable() {
		Enumeration<Auction> enumeration = table.elements();
		Auction auction;
		
		if(!enumeration.hasMoreElements()) {
			System.out.println("The auction is currently empty.");
			return;
		}

        ArrayList<Auction> list = Collections.list(enumeration);
        Collections.sort(list);
        Collections.reverse(list);
 
		System.out.println(" Auction ID |      Bid   |        Seller         |          Buyer          |    Time   |  Item Info");
		System.out.println("=================================================================================================================================== ");
		
		for(int i=0; i<list.size(); i++) {
			auction = list.get(i);
			
			System.out.printf("%11s | $%9s | %-21s |  %-21s  | %9s | %-42s\n", truncate(auction.getAuctionID(), -9),
					(auction.getCurrentBid() == 0 ? "" : String.format("%,.2f", auction.getCurrentBid())),
					truncate(auction.getSellerName(), 21), truncate(auction.getBuyerName(), 21),
					auction.getTimeRemaining() + " hours", truncate(auction.getItemInfo(), 42));
		}
	}

	/**
	 * Shortens the length of the input text if it exceeds the input maxLength
	 * If maxLength is negative, it will truncate from right to left
	 * 
	 * @param text
	 * 	The String to truncate
	 * 
	 * @param maxLength
	 * 	The maximum length of the text to return
	 */
	public static String truncate(String text, int maxLength) {
		if(Math.abs(maxLength) > text.length()) {
			return text;
		} else if(maxLength < 0) {
			return text.substring(text.length() + maxLength, text.length());
		} else {
			return text.substring(0, maxLength);
		}
	}

	/**
	 * Saves the input AuctionTable to the fileName
	 * 
	 * @param fileName
	 * 	The file to save the AuctionTable to
	 * 
	 * @param auctions
	 * 	The AuctionTable to save
	 * 
	 * @throws IOException
	 * 	Thrown if the file could not be written to
	 * 
	 * <dt><b>Postcondition:</b><dd>
	 *  A file with fileName will now contain the AuctionTable
	 */
	public static void saveAuctionTable(String fileName, AuctionTable auctions) throws IOException {
		FileOutputStream file = new FileOutputStream(fileName);
		ObjectOutputStream outStream = new ObjectOutputStream(file);
		outStream.writeObject(auctions);
		
		outStream.close();
	}

	/**
	 * Loads and returns the AuctionTable saved at fileName
	 * 
	 * @param fileName
	 * 	The file to load the AuctionTable from
	 * 
	 * @throws IOException
	 * 	Thrown if the file could not be loaded
	 * 
	 * @throws ClassNotFoundException
	 * 	Thrown if the AuctionTable class could not be found
	 * 
	 * @return
	 * 	The loaded AuctionTable
	 */
	public static AuctionTable loadAuctionTable(String fileName) throws IOException, ClassNotFoundException {
		FileInputStream file = new FileInputStream(fileName);
		ObjectInputStream inStream = new ObjectInputStream(file);
		AuctionTable auctions = (AuctionTable)inStream.readObject();
		
		inStream.close();
		return auctions;
	}
}
