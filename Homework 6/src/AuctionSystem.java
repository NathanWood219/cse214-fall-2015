/**
* The <code>AuctionSystem</code> class uses provides a menu to handle user navigation of the custom auction.
* 
* @author Nathan Wood
*    e-mail: nathan.wood@stonybrook.edu
*    Stony Brook ID: 109876625
 * <dt><b>Assignment:</b><dd>
 *    Homework #6 for CSE 214, Fall 2015
 * <dt><b>Date:</b><dd>
 *    November 17th, 2015
 **/

import java.io.IOException;
import java.util.InputMismatchException;
import java.util.Scanner;

public class AuctionSystem {
	public static void main(String args[] ) {
		Scanner input = new Scanner(System.in);
		System.out.println("Starting...");
		
		AuctionTable auctionTable;
		String username;
		
		// Attempt to load the AuctionTable
		try {
			auctionTable = AuctionTable.loadAuctionTable("auction.obj");
			System.out.println("Successfully loaded auction table.");
			
		} catch(IOException|ClassNotFoundException e) {
			System.out.println("No previous auction table detected.");
			System.out.println("Creating new table...");
			
			auctionTable = new AuctionTable();
		}
		
		// Get a username from the user
		System.out.print("\nPlease select a username: ");
		username = input.nextLine();
		
		String userInput;
		boolean printMenu = true;
		
		// Continue to loop until the user requests to quit
		while(true) {
			if(printMenu) {
				System.out.println("\nMenu:\n\t(D) - Import Data from URL\n\t(A) - Create a New Auction\n\t(B) - Bid on an Item" + 
						"\n\t(I) - Get Info on Auction\n\t(P) - Print All Auctions\n\t(R) - Remove Expired Auctions" + 
						"\n\t(T) - Let Time Pass\n\t(Q) - Quit\n\n");
			}
			
			printMenu = true;
			
			System.out.print("Please select an option: ");
			userInput = input.nextLine().toUpperCase();
			
			if(userInput.equals("D")) {														// (D) - Import Data from URL
				System.out.print("Please enter a URL: ");
				userInput = input.nextLine();
				System.out.println("\nLoading...");
				
				try {
					auctionTable = AuctionTable.buildFromURL(userInput);
					
					System.out.println("Auction data loaded successfully!");
				} catch(Exception e) {
					System.out.println("Unable to load auction data.");
				}
				
			} else if(userInput.equals("A")) {												// (A) - Create a New Auction
				
				try {
					System.out.println("Creating new Auction as " + username);
					
					System.out.print("Please enter an Auction ID: ");
					String auctionID = input.nextLine();
					Long.parseLong(auctionID);
					
					System.out.print("Please enter an Auction time (hours): ");
					int auctionTime = input.nextInt();
					
					input.nextLine();
					
					System.out.print("Please enter some Item Info: ");
					String auctionInfo = input.nextLine();
					
					auctionTable.putAuction(auctionID, new Auction(auctionTime, auctionID, username, auctionInfo));
					
					System.out.println("\nAuction " + auctionID + " inserted into table.");
					
				} catch(NumberFormatException|NullPointerException e) {
					System.out.println("ERROR: Auction ID must contain only digits.");
				} catch(InputMismatchException e) {
					System.out.println("ERROR: Time remaining must be an integer.");
					input.nextLine();
				}
				
			} else if(userInput.equals("B")) {												// (B) - Bid on an Item
				
				System.out.print("Please enter an Auction ID: ");
				userInput = input.nextLine();
				Auction auction = auctionTable.getAuction(userInput);
				
				if(auction == null) {
					System.out.println("\nAuction " + userInput + " could not be found.");
				} else {
					System.out.println("\nAuction " + userInput + " is "
							+ (auction.getTimeRemaining()==0 ? "CLOSED" : "OPEN"));
					System.out.println("    Current Bid: $" + String.format("%,.2f",auction.getCurrentBid()));
					
					if(auction.getTimeRemaining() == 0) {
						System.out.println("\nYou can no longer bid on this item.");
					} else {
						System.out.print("\nWhat would you like to bid?: ");
						
						try {						
							if(auction.newBid(username, input.nextDouble())) {
								System.out.println("Bid accepted.");
							} else {
								System.out.println("Bid rejected.");
							}
						} catch(ClosedAuctionException e) {
							System.out.println("ERROR: Cannot bid on a closed auction.");
						} catch(InputMismatchException e) {
							System.out.println("ERROR: Input must be a double.");
						}
						
						input.nextLine();
					}
					
				}
				
			} else if(userInput.equals("I")) {												// (I) - Get Info on Auction
				
				System.out.print("Please enter an auction ID: ");
				userInput = input.nextLine();
				Auction auction = auctionTable.getAuction(userInput);
				
				if(auction == null) {
					System.out.println("\nAuction " + userInput + " could not be found.");
				} else {
					System.out.println("\n" + auction);
				}
				
			} else if(userInput.equals("P")) {												// (P) - Print All Auctions
				
				System.out.println();
				auctionTable.printTable();
				
			} else if(userInput.equals("R")) {												// (R) - Remove Expired Auctions
				
				System.out.println("Removing expired auctions...");
				auctionTable.removeExpiredAuctions();
				System.out.println("All expired auctions removed.");
				
			} else if(userInput.equals("T")) {												// (T) - Let Time Pass
				
				try {
					System.out.print("How many hours should pass: ");
					int hours = input.nextInt();
	
					System.out.println("\nTime passing...");
					
					auctionTable.letTimePass(hours);
					System.out.println("Auction times updated.");
				} catch(IllegalArgumentException|InputMismatchException e) {
					System.out.println("ERROR: The number of hours must be a positive integer.");
				}
				
				input.nextLine();
				
			} else if(userInput.equals("Q")) {												// (Q) - Quit
				
				break;
				
			} else {																		// Faulty input
				
				System.out.println("ERROR: Unknown command.\n");
				printMenu = false;
				
			}
		}
		
		System.out.println("\nWriting auction table to file...");
		
		// Attempt to save the AuctionTable
		try {
			AuctionTable.saveAuctionTable("auction.obj", auctionTable);
			System.out.println("Done!");
		} catch(IOException e) {
			System.out.println("ERROR: Unable to serialize the auction table.");
		}
		
		System.out.println("\nGoodbye.");
		input.close();
	}
}
