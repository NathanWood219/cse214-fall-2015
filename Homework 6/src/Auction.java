/**
* The <code>Auction</code> class contains the data fields and methods required to maintain a single auction.
* 
* @author Nathan Wood
*    e-mail: nathan.wood@stonybrook.edu
*    Stony Brook ID: 109876625
 * <dt><b>Assignment:</b><dd>
 *    Homework #6 for CSE 214, Fall 2015
 * <dt><b>Date:</b><dd>
 *    November 17th, 2015
 **/

import java.io.Serializable;

public class Auction implements Serializable, Comparable<Auction> {
	private static final long serialVersionUID = 1L;
	
	// ATTRIBUTES
	private int timeRemaining = 0;
	private double currentBid = 0;
	private String auctionID = "", sellerName = "", buyerName = "", itemInfo = "";
	
	// CONSTRUCTORS

	/**
	 * Default constructor for an Auction object
	 * 
	 * <dt><b>Postcondition:</b><dd>
	 * 	This Auction object will have been initialized with all the default attributes
	 */
	public Auction() {
	}

	/**
	 * Secondary constructor for an Auction object with parameters
	 * 
	 * @param timeRemaining
	 * 	The number of hours remaining
	 * 
	 * @param currentBid
	 * 	The current bid amount in dollars
	 * 
	 * @param auctionID
	 * 	The ID of this Auction
	 * 
	 * @param sellerName
	 *  The name of the seller
	 * 
	 * @param buyerName
	 * 	The name of the highest bidder
	 * 
	 * @param itemInfo
	 * 	The description of the item being sold
	 * 
	 * <dt><b>Postcondition:</b><dd>
	 * 	This Auction object will have been initialized with all the input parameters
	 */
	public Auction(int timeRemaining, double currentBid, String auctionID,
			String sellerName, String buyerName, String itemInfo) {
		
		this.timeRemaining = timeRemaining;
		this.currentBid = currentBid;
		this.auctionID = auctionID;
		this.sellerName = sellerName;
		this.buyerName = buyerName;
		this.itemInfo = itemInfo;
		
	}

	/**
	 * An extra constructor for an Auction object with specific parameters
	 * 
	 * @param timeRemaining
	 * 	The number of hours remaining
	 * 
	 * @param auctionID
	 * 	The ID of this Auction
	 * 
	 * @param sellerName
	 *  The name of the seller
	 * 
	 * @param itemInfo
	 * 	The description of the item being sold
	 * 
	 * <dt><b>Postcondition:</b><dd>
	 * 	This Auction object will have been initialized with all the input parameters
	 */
	
	public Auction(int timeRemaining, String auctionID, String sellerName, String itemInfo) {
		
		this.timeRemaining = timeRemaining;
		this.auctionID = auctionID;
		this.sellerName = sellerName;
		this.itemInfo = itemInfo;
		
	}
	
	// ACCESSORS
	
	/**
	 * Returns the time remaining for this Auction
	 * 
	 * @return
	 * 	The time left before this Auction expires
	 */
	public int getTimeRemaining() {
		return timeRemaining;
	}
	
	/**
	 * Returns the highest bid amount placed on this Auction
	 * 
	 * @return
	 * 	The current highest bid amount
	 */
	public double getCurrentBid() {
		return currentBid;
	}
	
	/**
	 * Returns this Auction's ID
	 * 
	 * @return
	 * 	The ID associated with this Auction
	 */
	public String getAuctionID() {
		return auctionID;
	}
	
	/**
	 * Returns the name of the seller of this Auction
	 * 
	 * @return
	 * 	The name of the seller
	 */
	public String getSellerName() {
		return sellerName;
	}
	
	/**
	 * Returns the name of the highest bidder
	 * 
	 * @return
	 * 	The name of the highest bidder
	 */
	public String getBuyerName() {
		return buyerName;
	}
	
	/**
	 * Returns the information about this Auction item
	 * 
	 * @return
	 * 	The item information for this Auction
	 */
	public String getItemInfo() {
		return itemInfo;
	}
	
	// METHODS
	
	/**
	 * Decreases the time remaining by the input amount in hours
	 * 
	 * @param time
	 * 	The child to add to this object
	 * 
	 * <dt><b>Postcondition:</b><dd>
	 * 	The time remaining for this Auction will have been decremented by the set amount,
	 * 	but no lower than 0
	 */
	public void decrementTimeRemaining(int time) {
		if(timeRemaining - time < 0) {
			timeRemaining = 0;
		} else {
			timeRemaining -= time;
		}
	}
	
	/**
	 * Attempts to add a new bid to this Auction. If the Auction is closed, an exception will be thrown.
	 * Returns false if the bid amount was less than the current highest bid amount.
	 * 
	 * @param bidderName
	 * 	The name of the individual making the new bid
	 * 
	 * @param bidAmount
	 * 	The amount of money the new bidder has put on this Auction
	 * 
	 * @throws ClosedAuctionException
	 * 	Thrown if the Auction has expired
	 * 
	 * @return
	 * 	Returns true if the bid amount was greater than the previous highest bid amount
	 * 
	 * <dt><b>Postcondition:</b><dd>
	 * 	The current bid amount will now reflect the input bidAmount, but only if the new bid was greater
	 */
	public boolean newBid(String bidderName, double bidAmount) throws ClosedAuctionException {
		if(timeRemaining <= 0) {
			throw new ClosedAuctionException();
		}
		
		if(bidAmount > currentBid) {
			currentBid = bidAmount;
			buyerName = bidderName;
			return true;
		}
		
		return false;
	}
	
	/**
	 * Implemented using the Comparable interface, this allows a list of Auction objects to be sorted easily.
	 * 
	 * @param other
	 * 	The Auction object to compare this one to
	 * 
	 * @return
	 * 	Returns a value from -1 to 1 based on the comparison of the two objects' IDs
	 */
	public int compareTo(Auction other) {
		long thisID = Long.parseLong(auctionID);
		long otherID = Long.parseLong(other.getAuctionID());
		
		if(thisID > otherID) {
			return 1;
		} else if(thisID < otherID) {
			return -1;
		} else {
			return 0;
		}
	}
	
	/**
	 * Returns a formatted String representation of this Auction's data fields
	 * 
	 * @return
	 * 	A String representing this Auction
	 */
	public String toString() {
		return "Auction " + auctionID + ":\n\tSeller: " + sellerName + "\n\tBuyer: " +
				buyerName + "\n\tTime: " + timeRemaining + " hours\n\tInfo: " + itemInfo;
	}
}
