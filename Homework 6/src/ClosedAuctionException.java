/**
* A <code>ClosedAuctionException</code> will be thrown if a bid is added to a closed auction.
* 
* @author Nathan Wood
*    e-mail: nathan.wood@stonybrook.edu
*    Stony Brook ID: 109876625
 * <dt><b>Assignment:</b><dd>
 *    Homework #6 for CSE 214, Fall 2015
 * <dt><b>Date:</b><dd>
 *    November 17th, 2015
 **/
public class ClosedAuctionException extends Exception {

	private static final long serialVersionUID = 7910150037208189791L;

	/**
     * Default constructor for a <CODE>ClosedAuctionException</CODE> that
     * passes a default string to the <CODE>Exception</CODE> class constructor.
     *
     * <dt><b>Postcondition:</b><dd>
     *    The object is created and contains the default message.
     */
	public ClosedAuctionException() {
		// Default message
		super("ERROR: The auction has closed.");
	}
	
    /**
     * Second constructor for the <CODE>ClosedAuctionException</CODE> that
     * passes a provided string to the <CODE>Exception</CODE> class constructor.
     *
     * @param message
     *    the message that the object is to contain
     * <dt><b>Postcondition:</b><dd>
     *    The object is created and contains the provided message.
     */
    public ClosedAuctionException(String message) {   
    	// Custom message
        super(message);
    }
}
