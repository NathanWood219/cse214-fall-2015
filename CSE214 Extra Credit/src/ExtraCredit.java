import java.util.Scanner;
import java.util.ArrayList;

public class ExtraCredit {
	public static void main(String args[] ) {
		Scanner input = new Scanner(System.in);
		BinaryTree tree = new BinaryTree();
		
		System.out.print("Number of elements in the tree: ");
		int size = input.nextInt();
		
		ArrayList<Integer> array = new ArrayList<Integer>();
		
		for(int i=0; i<size; i++) {
			int temp = (int)(Math.random() * size);
			
			while(array.contains(temp)) {
				temp = (int)(Math.random() * size);
			}
			
			array.add(temp);
			tree.add(temp);
		}
		
		System.out.println("Inserting...");
		
		System.out.println("\nPreorder Traversal:");
		tree.printPreorder();
		
		System.out.println("\nPerforming Inorder Numbering...");
		tree.numberNodes();
		System.out.println("\nInorder Traversal:");
		tree.printInorder();
		System.out.println("\nPreorder Traversal:");
		tree.printPreorder();
		
		System.out.println("\n[Note: Could not get recursive removal to work correctly.]");
		
		System.out.print("\nNumber to remove: ");
		tree.remove(input.nextInt());
		
		System.out.println("\nInorder Traversal: ");
		tree.printInorder();
		
		System.out.println("\nPreorder Traversal:");
		tree.printPreorder();
		
		input.close();
	}
}
