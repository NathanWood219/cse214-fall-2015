import java.util.Stack;

public class BinaryTree {
	// ATTRIBUTES
	private BTNode root;
	
	// CONSTRUCTORS
	
	public BinaryTree() {
	}
	
	// METHODS
	
	// Nonrecursive
	public void add(int data) {
		if(root == null) {
			root = new BTNode(null, data);
			return;
		}
		
		BTNode temp = root;
		
		while(true) {
			if(data < temp.getData()) {
				if(temp.getLeft() == null) {
					temp.setLeft(new BTNode(temp, data));
					return;
				} else {
					temp = temp.getLeft();
				}
				
			} else if(data > temp.getData()) {
				if(temp.getRight() == null) {
					temp.setRight(new BTNode(temp, data));
					return;
				} else {
					temp = temp.getRight();
				}
			} else {
				return;
			}
		}
	}
	
	// Starts a recursive call
	public void remove(int data) {
		root.remove(data);
	}
	
	// Nonrecursive
	public void printPreorder() {
		if(root == null) {
			return;
		}
		
		Stack<BTNode> elements = new Stack<BTNode>();
		
		elements.push(root);
		
		while(!elements.isEmpty()) {
			BTNode temp = elements.pop();
			
			System.out.print(temp + " ");
			
			if(temp.getRight() != null) {
				elements.push(temp.getRight());
			}
			
			if(temp.getLeft() != null) {
				elements.push(temp.getLeft());
			}
		}
		
		System.out.print("\n");
	}
	
	public void printInorder() {
		root.printInorder();
		System.out.print("\n");
	}
	
	public void numberNodes() {
		root.numberNodes();
	}
}
