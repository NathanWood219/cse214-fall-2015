
public class WeightsOptimization {
	public static void main(String args[] ) {
		int[] weights = {3, 2, 1};
		int capacity = 5;
		
		printWeightCombinations(weights, capacity);
	}
	
	
	// Make it similar to the tent problem, going forwards and backwards in the 1D array
	public static boolean printWeightCombinations(int[] weights, int capacity) {
		int combinations = fact(weights.length);
		int index = 0;
		
		return true;
	}
	
	public static int fact(int n) {
		if(n==0) {
			return 1;
		} else {
			return n * fact(n-1);
		}
	} 
}
