
public class BTNode {
	// ATTRIBUTES
	private BTNode left, right, parent;
	private int data;
	
	// CONSTRUCTORS
	
	public BTNode(BTNode parent, int data) {
		this.data = data;
	}
	
	// ACCESSORS
	
	public BTNode getLeft() {
		return left;
	}
	
	public BTNode getRight() {
		return right;
	}
	
	public BTNode getParent() {
		return parent;
	}
	
	public int getData() {
		return data;
	}
	
	// MUTATORS
	
	public void setLeft(BTNode left) {
		this.left = left;
	}
	
	public void setRight(BTNode right) {
		this.right = right;
	}
	
	public void setParent(BTNode parent) {
		this.parent = parent;
	}
	
	public void setData(int data) {
		this.data = data;
	}
	
	// METHODS
	
	// Recursive
	public void remove(int data) {
		// Case 1: This is the node to remove
		if(data == this.data) {
			// Case 1.1: Both children are null
			if(left == null && right == null && parent != null) {
				if(parent.getLeft() != null && parent.getLeft() == this) {
					parent.setLeft(null);
				} else {
					parent.setRight(null);
				}
			}
			// Case 1.2: Neither children are null
			else if(left != null && right != null) {
				BTNode rightmost = left.getRightmost();
				this.data = rightmost.getData();
				this.left = rightmost.getLeft();
				if(rightmost.getParent() != null) {
					rightmost.getParent().setRight(null);
				}
			}
			// Cases 1.3 & 1.4: only one node is null
			else if(parent != null){
				BTNode node = (left == null ? right : left);
				if(parent.getLeft() != null && parent.getLeft() == this) {
					parent.setLeft(node);
				} else {
					parent.setRight(node);
				}
			}
		}
		// Case 2: This data is less than the input data
		else if(data < this.data && left != null) {
			left.remove(data);
		}
		// Case 3: This data is greater than the input data
		else if(right != null) {
			right.remove(data);
		}
	}
	
	// Recursive
	public BTNode getRightmost() {
		if(right == null) {
			return this;
		} else {
			return right.getRightmost();
		}
	}
	
	// Inorder, helper method
	public void numberNodes() {
		numberNodes(0);
	}
	
	// Inorder, recursive
	private int numberNodes(int value) {
		if(left != null) {
			value = left.numberNodes(value) + 1;
		}
		
		data = value;
		
		if(right != null) {
			value = right.numberNodes(value+1);
		}
		
		return value;
	}
	
	public void printInorder() {
		if(left != null) {
			left.printInorder();
		}
		
		System.out.print(data + " ");
		
		if(right != null) {
			right.printInorder();
		}
	}
	
	public String toString() {
		return Integer.toString(data);
	}
}
