
public class Stack implements Cloneable {
	private int size, capacity;
	private Object[] stack;
	
	Stack() {
		this.size = 0;
		this.capacity = 1;
		this.stack = new Object[capacity+1];
	}
	
	// int size()
	public int size() {
		return size;
	}
	
	// boolean empty()
	public boolean empty() {
		return (size == 0);
	}
	
	// void push(Object o)
	public void push(Object element) {
		size++;
		if(size==capacity) {
			capacity *= 2;
			Object[] temp = new Object[capacity+1];
			for(int i=0; i<size-1; i++) {
				temp[i] = stack[i];
			}
			stack = temp;
		}
		stack[size-1] = element;
	}
	
	// Object peek()
	public Object peek() {
		return stack[size-1];
	}
	
	// Object pop()
	public Object pop() {
		return stack[--size];
	}
	
	// void trim()
	public void trim() {
		Object[] tempStack = new Object[size];
		for(int i=0; i<size; i++) {
			tempStack[i] = stack[i];
		}
		
		stack = tempStack;
		capacity = size;
	}
	
	// int indexOf(Object o)
	public int indexOf(Object o) {
		for(int i=0; i<size; i++) {
			if(stack[i]==o) {
				return i;
			}
		}
		
		return -1;
	}
	
	public Object clone() {
		Stack newStack = new Stack();
		try {
			newStack = (Stack)super.clone();
		} catch(CloneNotSupportedException e) {
			System.out.println("Clone not supported.");
		}
		
		return newStack;
	}
	
	// Object elementAt(int position)
	public Object elementAt(int i) {
		return stack[i];
	}
	
	// Object[] getStack()
	public Object[] getStack() {
		return stack;
	}
	
	public boolean isEmpty() {
		return (size==0);
	}
}
