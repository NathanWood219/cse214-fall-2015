// 14 29 57

/*
Amount: $1-100
3 money orders
Find the 3 amounts that would have the smallest possible loss

x + y + z >= 100
	The sum of the three orders must be at least $100
	but if the sum is greater than $100 then the losses add up even faster
	
This program takes three values as an input and calculates the total potential loss
Therefore the three values that have the smallest total potential lost are optimal
	
Base point for testing: 20 30 50

Log of test cases:
	20 30 50 --> 850
	10 30 60 --> 750
	15 25 60 --> 700
	12 39 58 --> 690
	14 26 60 --> 686
	13 27 60 --> 684
	16 28 55 --> 681
	15 30 55 --> 675
	13 30 57 --> 675
	13 29 58 --> 672
	14 30 56 --> 670
	15 29 56 --> 668
	15 28 57 --> 667
	14 29 57 --> 665 	[THIS IS THE MOST OPTIMAL SET OF VALUES]
	
Answer: $14, $29, $57


Automating it:
	for(int x=1; x<=50; x++) {
		for(int y=1; y<=49; y++) {
			z = 100 - (x + y);
			
			// LIM amount -> 100
			for(int amount=1; amount<=LIMIT; amount++) {
				sumOfLosses += getLoss(amount,x,y,z);
			}
			
			currentLine = intOrder + " --> $" + sumOfLosses;
			
			recent.push(currentLine);
			log.add(currentLine);
			
			System.out.println("\n" + currentLine);
				
			
			

*/

/* TO-DO:
	1. Updating the text file prints the latest stuff at the start instead of merging the arrays properly
*/

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class MoneyOrdersProblem {
	@SuppressWarnings("unchecked")
	public static void main(String args[] ) {
		Scanner input = new Scanner(System.in);
		String currentLine = "", intOrder = "";
		Stack recent = new Stack();
		ArrayList log = new ArrayList();
		
		final int LIMIT = 100;
		int x = 20, y = 30, z = 50;
		int sumOfLosses = 0;
		
		System.out.println("[entering something other than an integer will terminate this session]");
		
		while(true) {
			try {				
				do {
					System.out.print("\nX: ");
					x = input.nextInt();
		
					System.out.print("Y: ");
					y = input.nextInt();
					
					//System.out.print("Z: ");
					//z = input.nextInt();
					
					z = 100 - (x + y);
				} while(z<=0);
				
				intOrder = getIntOrder(x,y,z);
				sumOfLosses = 0;
				
				if(x+y+z<100) {
					System.out.println("The sum must be at least 100.\n");
				}
				else {
					// LIM amount -> 100
					for(int amount=1; amount<=LIMIT; amount++) {
						sumOfLosses += getLoss(amount,x,y,z);
					}
					
					currentLine = intOrder + " --> $" + sumOfLosses;
					
					recent.push(currentLine);
					log.add(currentLine);
					
					System.out.println();
					printStringStack(recent);
				}
			} catch(Exception e) {
				break;
			}
		}
		
		System.out.println("\n ---- LOG ----");
		log = sortStringArray(log);
		printStringArray(log);
		
		System.out.println("\nUpdating log text file...");
		updateTestLog(log);
		
		System.out.println("\nProgram terminating...");
		input.close();
	}
	
	public static int getLoss(int amount, int x, int y, int z) {		
		int[] array = {101,101,101,101,101,101,101,101};
		
		int i = 0;
		if(x>=amount) {array[i++]=x;}
		if(y>=amount) {array[i++]=y;}
		if(z>=amount) {array[i++]=z;}
		if(x+y>=amount) {array[i++]=x+y;}
		if(x+z>=amount) {array[i++]=x+z;}
		if(y+z>=amount) {array[i++]=y+z;}
		if(x+y+z>=amount) {array[i++]=x+y+z;}
		
		Arrays.sort(array);
		
		return Math.abs(array[0] - amount);
	}
	
	public static void printStringStack(Stack stack) {
		Stack newStack = (Stack)stack.clone();
		int range = (stack.size()<=5 ? stack.size() : 5);
		String output = "";
		
		for(int i=0; i<range; i++) {
			output = newStack.pop() + "\n" + output;
			
		}
		System.out.println(output.substring(0,output.length()-1));
	}
	
	public static void printStringArray(ArrayList array) {
		String output = "";
		for(int i=0; i<array.size(); i++) {
			output += array.get(i) + "\n";
		}
		System.out.println(output.substring(0,output.length()-1));
	}
	
	@SuppressWarnings("unchecked")
	public static ArrayList sortStringArray(ArrayList array) {
		int high, highIndex = 0, current, size = array.size();
		ArrayList tempArray = new ArrayList();
		boolean[] isEmpty = new boolean[size];
		String line = "";
		
		for(int i=0; i<size; i++) {
			isEmpty[i] = false;
		}
		
		highIndex = getFirstFalse(isEmpty);
		
		while(highIndex!=-1) {			
			line = (String)array.get(highIndex);
			high = Integer.parseInt(line.substring(line.indexOf("$")+1, line.length()));
					
			for(int k=0; k<size; k++) {
				if(isEmpty[k]==false) {
					line = (String)array.get(k);
					current = Integer.parseInt(line.substring(line.indexOf("$")+1, line.length()));
					
					if(current>high) {
						highIndex = k;
						high = current;
					}
				}
			}
			
			isEmpty[highIndex] = true;
			if(!tempArray.contains((String)array.get(highIndex))) {
				tempArray.add((String)array.get(highIndex));
			}

			highIndex = getFirstFalse(isEmpty);
		}
		
		return tempArray;
	}

	public static int getFirstFalse(boolean[] array) {
		for(int i=0; i<array.length; i++) {
			if(array[i]==false) {
				return i;
			}
		}
		
		return -1;
	}

	public static String getIntOrder(int x, int y, int z) {
		int[] array = {x,y,z};
		
		Arrays.sort(array);
		return String.format("%02d %02d %02d", array[0], array[1], array[2]);
	}
	
	@SuppressWarnings("unchecked")
	public static void updateTestLog(ArrayList array) {
		String line = null;
		
		try {
			FileReader readText = new FileReader("C:\\Users\\Nathan\\Desktop\\Nathan\\Software\\Java\\MoneyOrdersLog.txt");
			
			BufferedReader bufferedReader = new BufferedReader(readText);
			
			line = bufferedReader.readLine();
			
			while(!(line = bufferedReader.readLine()).equals("-----------------") && line!=null) {
                array.add(line);
            }
			
			bufferedReader.close();
			
			sortStringArray(array);
			
			FileWriter writeText = new FileWriter("C:\\Users\\Nathan\\Desktop\\Nathan\\Software\\Java\\MoneyOrdersLog.txt");
			BufferedWriter bufferedWriter = new BufferedWriter(writeText);
			
			bufferedWriter.write("x  y  z      loss");
			bufferedWriter.newLine();
			
			for(int i=0; i<array.size(); i++) {
				bufferedWriter.write((String)array.get(i));
				bufferedWriter.newLine();
			}
			
			String lastLine = (String)array.get(array.size()-1);
			
			bufferedWriter.write("-----------------");
			bufferedWriter.newLine();
			bufferedWriter.write("Optimal: " + lastLine.substring(0, lastLine.indexOf("$")-4));
			
			System.out.println("Log text file successfully updated.");
			
			bufferedWriter.close();
		} catch (FileNotFoundException e) {
			System.out.println("\n Unable to open the log text file.");
		} catch(IOException e) {
			System.out.println("\n Error reading the log text file.");
		}
	}
}
