// 14 29 57

/*
Amount: $1-100
3 money orders
Find the 3 amounts that would have the smallest possible loss

x + y + z = 100
	The sum of the three orders must be at least $100
	but if the sum is greater than $100 then the losses add up even faster
	
z = 100 - (x + y)
	
This program takes three values as an input and calculates the total potential loss
Therefore the three values that have the smallest total potential lost are optimal
	
Base point for testing: 20 30 50

Log of 10 best cases:
	13 30 57 --> $675
	15 30 55 --> $675
	14 27 59 --> $673
	13 29 58 --> $672
	15 27 58 --> $672
	14 30 56 --> $670
	15 29 56 --> $668
	15 28 57 --> $667
	14 28 58 --> $666
	14 29 57 --> $665 	[THIS IS THE MOST OPTIMAL SET OF VALUES]
	
Answer: $14, $29, $57
*/

/* TO-DO:
	1. Updating the text file prints the latest stuff at the start instead of merging the arrays properly
*/

import java.util.ArrayList;
import java.util.Arrays;

public class MoneyOrdersAuto {
	@SuppressWarnings("unchecked")
	public static void main(String args[] ) {
		String currentLine = "", intOrder = "";
		ArrayList log = new ArrayList();
		
		final int LIMIT = 100;
		final int RANGE = 50;
		int steps = 0;
		double percentage;
		int z, sumOfLosses = 0;
		
		System.out.print("Progress: ");
		
		for(int x=1; x<=RANGE; x++) {
			for(int y=1; y<RANGE; y++) {
				z = 100 - (x + y);
				sumOfLosses = 0;
				
				// LIM amount -> 100
				for(int amount=1; amount<=LIMIT; amount++) {
					sumOfLosses += getLoss(amount,x,y,z);
					steps++;
				}
				
				intOrder = getIntOrder(x,y,z);
				
				currentLine = intOrder + " --> $" + sumOfLosses;
				
				log.add(currentLine);
			}
			
			percentage = 100.0*(1.0*x)/(RANGE);
			
			if(Math.round(percentage/5)*5%5==0 && percentage>0.1) {				
				System.out.printf("%03.2f%%, ",percentage);
			}
			
			if(x%10==0) {
				System.out.print("\n\t  ");
			}
		}	

		
		System.out.printf("\nCompleted in %,d steps.\n",steps);
		System.out.printf("Sorted through %,d possible combinations.\n\n",(RANGE*RANGE));
		System.out.println(" --- TOP TEN --- ");
		printLastTen(sortStringArray(log));
		
		System.out.println("\nProgram terminating...");
	}
	
	public static int getLoss(int amount, int x, int y, int z) {		
		int[] array = {101,101,101,101,101,101,101,101};
		
		int i = 0;
		if(x>=amount) {array[i++]=x;}
		if(y>=amount) {array[i++]=y;}
		if(z>=amount) {array[i++]=z;}
		if(x+y>=amount) {array[i++]=x+y;}
		if(x+z>=amount) {array[i++]=x+z;}
		if(y+z>=amount) {array[i++]=y+z;}
		if(x+y+z>=amount) {array[i++]=x+y+z;}
		
		Arrays.sort(array);
		
		return Math.abs(array[0] - amount);
	}
	
	public static void printLastTen(ArrayList array) {
		String output = "", answer = "";
		for(int i=array.size()-11; i<array.size(); i++) {
			output += array.get(i) + "\n";
		}

		answer = (String)array.get(array.size()-1);
		output += "-----------------\nAnswer: " + answer.substring(0, answer.indexOf("$")-4);
		System.out.println(output.substring(0,output.length()-1));
	}
	
	@SuppressWarnings("unchecked")
	public static ArrayList sortStringArray(ArrayList array) {
		int high, highIndex = 0, current, size = array.size();
		ArrayList tempArray = new ArrayList();
		boolean[] isEmpty = new boolean[size];
		String line = "";
		
		for(int i=0; i<size; i++) {
			isEmpty[i] = false;
		}
		
		highIndex = getFirstFalse(isEmpty);
		
		while(highIndex!=-1) {			
			line = (String)array.get(highIndex);
			high = Integer.parseInt(line.substring(line.indexOf("$")+1, line.length()));
					
			for(int k=0; k<size; k++) {
				if(isEmpty[k]==false) {
					line = (String)array.get(k);
					current = Integer.parseInt(line.substring(line.indexOf("$")+1, line.length()));
					
					if(current>high) {
						highIndex = k;
						high = current;
					}
				}
			}
			
			isEmpty[highIndex] = true;
			if(!tempArray.contains((String)array.get(highIndex))) {
				tempArray.add((String)array.get(highIndex));
			}

			highIndex = getFirstFalse(isEmpty);
		}
		
		return tempArray;
	}

	public static int getFirstFalse(boolean[] array) {
		for(int i=0; i<array.length; i++) {
			if(array[i]==false) {
				return i;
			}
		}
		
		return -1;
	}

	public static String getIntOrder(int x, int y, int z) {
		int[] array = {x,y,z};
		
		Arrays.sort(array);
		return String.format("%02d %02d %02d", array[0], array[1], array[2]);
	}
}