# This function prints some statements in O(n * log(n)) time.
def test_function(n):

    # Get the range [0, n-1].
    N = xrange(n)
    # Get the range [0, floor(log(n))-1] 
    log_N = xrange(int(math.log(n, 2)))

    # Stack record that loops from 0 to n-1.
    for i in N:

        for s in log_N:
            print("This will print n * log(n) times.")

            # Nested stack record that loops from 0 to floor(log(n))-1.
            for m in log_N:

                print("This statement prints n * log(n^2) times.")

        for p in N:
            print("Printing this stuff n^2")

        t = n
        while t > 1:
            print("n times for this one too")
            n -= 1

    # All 'while' statements will have a variable go from 'n' to 1.
    k = n
    while k > 1:

        print("But this statement only prints log(n) times.")

        j = n
        while j > 1:
            j /= 2

        # But you will have to determine the order from the
        # update statement (log(n), in this case).
        k /= 2 

    print("Since n * log(n) is bigger complexity, the whole "
          "funtion is O(n * log(n).")
