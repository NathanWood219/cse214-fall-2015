/**
* The <code>BlockStack</code> class is a custom Stack that handles BlockStack objects
*    
* @author Nathan Wood
*    e-mail: nathan.wood@stonybrook.edu
*    Stony Brook ID: 109876625
* <dt><b>Assignment:</b><dd>
*    Homework #3 for CSE 214, Fall 2015
* <dt><b>Date:</b><dd>
*    October 6th, 2015
**/

public class BlockStack {
	// ATTRIBUTES
	private int size = 0, capacity = 1;
	private CodeBlock[] stack;
	
	// CONSTRUCTORS
	
	/**
	 * Default constructor for a BlockStack object
	 * 
	 * <dt><b>Postcondition:</b><dd>
	 * 	This BlockStack will have been initialized with the default capacity
	 */
	public BlockStack() {
		this.stack = new CodeBlock[capacity+1];
	}
	
	// METHODS
	
	/**
	 * Pushes a new CodeBlock onto the top of the BlockStack
	 * 
	 * @param element
	 * 	The CodeBlock object to be pushed
	 * 
	 * <dt><b>Postcondition:</b><dd>
	 * 	The size of the BlockStack will be incremented and the new CodeBlock will be at the top
	 */
	public void push(CodeBlock element) {
		size++;
		if(size==capacity) {
			capacity *= 2;
			CodeBlock[] temp = new CodeBlock[capacity+1];
			for(int i=0; i<size-1; i++) {
				temp[i] = stack[i];
			}
			stack = temp;
		}
		stack[size-1] = element;
	}
	
	/**
	 * Returns the CodeBlock at the top of the BlockStack
	 * 
	 * @return
	 * 	The CodeBlock at the top
	 */
	public CodeBlock peek() {
		return stack[size-1];
	}
	
	/**
	 * Removes and returns the CodeBlock at the top of the BlockStack
	 * 
	 * @return
	 * 	The CodeBlock at the top
	 * 
	 * <dt><b>Postcondition:</b><dd>
	 * 	The size will be decremented
	 */
	public CodeBlock pop() {
		return stack[--size];
	}

	/**
	 * Returns the size of the BlockStack
	 * 
	 * @return
	 * 	The size of the BlockStack
	 */
	public int size() {
		return size;
	}

	/**
	 * Returns whether the BlockStack is empty or not
	 * 
	 * @return
	 * 	True if the size of BlockStack is equal to 0
	 */
	public boolean isEmpty() {
		return (size == 0);
	}
}
