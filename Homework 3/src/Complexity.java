/**
* The <code>Complexity</code> class stores the power and log of each block of code to determine the order of complexity.
*    
* @author Nathan Wood
*    e-mail: nathan.wood@stonybrook.edu
*    Stony Brook ID: 109876625
* <dt><b>Assignment:</b><dd>
*    Homework #3 for CSE 214, Fall 2015
* <dt><b>Date:</b><dd>
*    October 6th, 2015
**/

public class Complexity {
	// ATTRIBUTES
	private int nPower = 0, logPower = 0;
	
	// CONSTRUCTORS
	
	/**
	 * Default constructor for a Complexity object
	 * 
	 * <dt><b>Postcondition:</b><dd>
	 * 	This Complexity object will have been initialized
	 */
	public Complexity() {
	}
	
	/**
	 * Secondary constructor for a Complexity object
	 * 
	 * @param nPower
	 * 	The number of times n is iterated through by subtracting
	 * 
	 * @param logPower
	 * 	The number of times n is iterated through by dividing by 2
	 * 
	 * <dt><b>Postcondition:</b><dd>
	 * 	This Complexity will have been initialized
	 */
	public Complexity(int nPower, int logPower) {
		this.nPower = nPower;
		this.logPower = logPower;
	}
	
	// ACCESSORS
	
	/**
	 * Returns the nPower of this Complexity object
	 * 
	 * @return
	 * 	The nPower of this Complexity
	 */
	public int getNPower() {
		return nPower;
	}

	/**
	 * Returns the logPower of this Complexity object
	 * 
	 * @return
	 * 	The logPower of this Complexity
	 */
	public int getLogPower() {
		return logPower;
	}
	
	// MUTATORS

	/**
	 * Sets the nPower of this Complexity
	 * 
	 * @param nPower
	 * 	The new nPower to set for this Complexity
	 */
	public void setNPower(int nPower) {
		this.nPower = nPower;
	}

	/**
	 * Sets the logPower of this Complexity
	 * 
	 * @param logPower
	 * 	The new logPower to set for this Complexity
	 */
	public void setLogPower(int logPower) {
		this.logPower = logPower;
	}
	
	// METHODS
	
	/**
	 * Returns a String representation of the object
	 * 
	 * @return
	 * 	A String containing the order of complexity for this Complexity object
	 */
	public String toString() {
		String output = "O(";
		
		if(nPower == 0 && logPower == 0) {
			output += "1";
		} else {
			output += (nPower>0 ? (nPower>1 ? "n^" + nPower : "n") : "");
			output += ((nPower>0 && logPower>0) ? " * " : "");
			output += (logPower>0 ?(logPower>1 ? "log(n)^" + logPower : "log(n)") : "");
		}
		
		return output + ")";
	}
}
