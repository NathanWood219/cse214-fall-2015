/**
* The <code>PythonTracer</code> class contains a method that reads in special Python code and returns the highest order of complexity.
*    
* @author Nathan Wood
*    e-mail: nathan.wood@stonybrook.edu
*    Stony Brook ID: 109876625
* <dt><b>Assignment:</b><dd>
*    Homework #3 for CSE 214, Fall 2015
* <dt><b>Date:</b><dd>
*    October 6th, 2015
**/

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

public class PythonTracer {
	// Number of spaces in an indent
	public static final int SPACE_COUNT = 4;
	
	// These variables are static so that traceFile() AND leaveCodeBlock() can read/write them
	// Each is set to their default value at the start of traceFile()
	static BlockStack stack;
	static String currentBlock;
	static int prevNum, prevLength;
	
	public static void main(String args[] ) {
		Scanner input = new Scanner(System.in);
		
		// Continue looping until broken
		while(true) {
			// Request a Python file name to parse
			System.out.print("Please enter a file name (or 'quit' to quit): ");
			String fileName = input.nextLine();
			
			// Break the while loop only if the user enters 'quit'
			if(fileName.toLowerCase().equals("quit")) {
				break;
			}
			
			// Add spacing
			System.out.println();
			
			try {
				// Attempt to trace the file
				Complexity solution = traceFile(fileName);
				
				// Print out the highest order of complexity if no exceptions were thrown
				System.out.println("Overall complexity of " + getFileName(fileName) + ": " + solution.toString() + "\n");
			} catch(FileNotFoundException e) {
				System.out.println("Error: file not found.\n");
			}
		}
		
		// This will only be reached if the user types 'quit'
		System.out.println("Program terminating successfully...");
		input.close();
	}

	/**
	 * Returns the Complexity of an input Python file, handling most exceptions on its own
	 * 
	 * @param fileName
	 * 	The file name of the Python file to trace
	 * 
	 * @throws FileNotFoundException
	 * 	Indicates that the param fileName did not exist
	 * 
	 * @return
	 * 	The highest Complexity of the Python code
	 */
	public static Complexity traceFile(String fileName) throws FileNotFoundException {
		String line;
		
		// Reset the static variables
		// These must be static so that both traceFile() and leaveCodeBlock() can access them
		stack = new BlockStack();
		currentBlock = "1";
		prevNum = 0;
		prevLength = 0;
		
		try {
			// Read the file
			FileInputStream fis = new FileInputStream(fileName);
			InputStreamReader inStream = new InputStreamReader(fis);
			BufferedReader reader = new BufferedReader(inStream);
			
			// While a next line exists, continue to parse the file
			while((line = reader.readLine()) != null) {
				
				// Checking if the line is empty
				if(line.isEmpty()) {
					continue;
				}
				
				// Count the number of indents
				int indents = countIndents(line);
				
				// Checking if the line is a comment or just contains indents
				if(line.charAt(indents*SPACE_COUNT)==35 || line.length()==(indents*SPACE_COUNT)) {
					continue;
				}
					
				// Continue to remove CodeBlocks until the blocks have collapsed to where the current indentation is positioned
				while(indents<stack.size()) {
					
					// Check if the code has been fully scanned
					if(indents==0 && stack.size()==1) {
						
						// Parsing has finished, close and return the highest complexity
						System.out.println("\tLeaving block 1.\n");
						reader.close();
						
						// Return the highest sub complexity
						return stack.peek().getHighestSubComplexity();
						
					} else {
						// Call the leaveCodeBlock() function, that will pass down highest order of complexity
						// 		as well as print the various updates to the console
						leaveCodeBlock();

						// Updating string for the next CodeBlock name
						if(currentBlock.length()>1) {
							// Store the previous length and last digit, then cut off the last two characters of the string
							prevLength = currentBlock.length();
							prevNum = Integer.parseInt(currentBlock.substring(currentBlock.length()-1,currentBlock.length()));
							currentBlock = currentBlock.substring(0,currentBlock.length()-2);
						} else {
							// Reset prevNum
							prevNum = 1;
						}
					}
				}
				
				String keyword;
				
				if(!(keyword = getKeyword(indents*SPACE_COUNT, line)).equals("")) {
					// ENTERING A NEW CODEBLOCK
					
					// Updating the CodeBlock name
					if(prevLength==currentBlock.length()+2) {

						currentBlock = currentBlock.substring(0,currentBlock.length()) + "." + Integer.toString(++prevNum);
					} else if(indents!=0){
						currentBlock += ".1";
					}
					
					System.out.println("\tEntering block " + currentBlock + " '" + keyword + "':");
					
					// Initialize a new Complexity and CodeBlock that will be added to the stack following the conditionals
					Complexity complexity = new Complexity();
					CodeBlock codeBlock = new CodeBlock(complexity, currentBlock);
					
					// Getting the complexity based on the keyword
					if(keyword.equals("for")) {
						
						// Found a for loop, determine the order of complexity and set it
						if(line.substring(line.length()-6,line.length()-1).equals("log_N")) {
							complexity.setLogPower(1);
						} else if(line.substring(line.length()-2,line.length()-1).equals("N")) {
							complexity.setNPower(1);
						}
						
					} else if(keyword.equals("while")) {
						
						// Found a while loop, store the loopVariable in the codeBlock
						int index = line.indexOf(keyword) + keyword.length() + 1;
						String loopVariable = line.substring(index,index+1);
						codeBlock.setLoopVariable(loopVariable);
						
					}
					
					// Add the new CodeBlock to the stack with the new Complexity and print the CodeBlock
					codeBlock.setBlockComplexity(complexity);
					stack.push(codeBlock);
					System.out.println(codeBlock.toString());
				} else {
					CodeBlock codeBlock = stack.peek();
					int index;
					
					// Check if the line contains the current CodeBlock's loopVariable
					if(codeBlock.getLoopVariable()!=null && (index = line.indexOf(codeBlock.getLoopVariable())) != -1) {
						
						// Found the loopVariable, inform the user
						System.out.println("\tFound update statement, updating block " + codeBlock.getName() + ":");
						
						//String operation = line.substring(index+2,index+6);
						Complexity complexity = codeBlock.getBlockComplexity();
						
						// Find the order of complexity
						if(line.contains("/= 2")) {
							// O(log n)
							complexity.setLogPower(complexity.getLogPower()+1);
							codeBlock.setBlockComplexity(complexity);
							
						} else if(line.contains("-= 1")) {
							// O(n)
							complexity.setNPower(complexity.getNPower()+1);
							codeBlock.setBlockComplexity(complexity);
							
						}
						
						// Print the updated order of complexity for the 'while' CodeBlock
						System.out.println(codeBlock.toString());
					}
				}	
			}
			
			while(stack.size()>1) {
				// Call the leaveCodeBlock() function, that will pass down highest order of complexity
				// 		as well as print the various updates to the console and update currentBlock string
				// This is being called here to make sure that all the blocks have been left, regardless of
				//		a sudden end to the Python file (resulting in it being empty)
				leaveCodeBlock();
			}
			
			// Done parsing the file, close and print a final statement
			reader.close();
			System.out.println("\tLeaving block 1.\n");
		} catch(FileNotFoundException e) {
			// Prevent the program from printing a redundant error
			if(stack.size()!=0) {
				System.out.println("Error: file not found.\n");
			}
		} catch(IOException e) {
			System.out.println("Error: input/output exception.\n");
		}
		
		// If stack size == 0, then the file never existed to begin with
		if(stack.size()==0) {
			throw new FileNotFoundException();
		}
		
		// Return the highest sub complexity
		return stack.peek().getHighestSubComplexity();
	}

	/**
	 * Leaves the current CodeBlock by popping it from the stack. Updates the highest order of complexity
	 * 	and prints out the updated CodeBlock to the user.
	 * 
	 * <dt><b>Precondition:</b><dd>
	 * 	This function can only be called from within traceFile(). The variables must have been initialized
	 * 	with the same names and the stack must have at least 1 more element.
	 * 
	 * <dt><b>Postcondition:</b><dd>
	 * 	The top element of stack will be removed and the new top highest sub complexity may have been updated
	 */
	public static void leaveCodeBlock() {
		// Pop and store the top CodeBlock in stack
		CodeBlock oldTop = stack.pop();
		
		// Store both complexities for the old top, as well as the highest complexity for the new top
		Complexity oldTopComplexity = oldTop.getBlockComplexity();
		Complexity oldTopHighestComplexity = oldTop.getHighestSubComplexity();
		Complexity newTopHighestComplexity = stack.peek().getHighestSubComplexity();
		
		// Compares just the two nPower because O(n) > O(log(n))
		// This simply allows the code to handle more unique cases than were given for testing
		if(oldTopComplexity.getNPower() + oldTopHighestComplexity.getNPower() >= newTopHighestComplexity.getNPower()) {
			
			// Add to the highest sub complexity of the new top
			stack.peek().setHighestSubComplexity(new Complexity(oldTopComplexity.getNPower() + oldTopHighestComplexity.getNPower(),
					oldTopComplexity.getLogPower() + oldTopHighestComplexity.getLogPower()));
			
			// Leaving block and updating the next one
			System.out.println("\tLeaving block " + oldTop.getName() + ", updating block " + stack.peek().getName() + ":");
		} else {
			// Leaving block and not changing anything
			System.out.println("\tLeaving block " + oldTop.getName() + ", nothing to update.");
		}

		// Print the toString() for the new top of stack
		System.out.println(stack.peek().toString());
	}
	
	/**
	 * Returns the number of indents in a line based on the static int SPACE_COUNT
	 * 
	 * @param line
	 * 	The String to scan
	 * 
	 * @return
	 * 	The number of indents before other text begins
	 */
	public static int countIndents(String line) {
		int i = 0;
		while(line.charAt(i)==32) {
			if(i+SPACE_COUNT>=line.length()) {
				return 0;
			}
			
			i += SPACE_COUNT;
		}
		return i/SPACE_COUNT;
	}
	
	/**
	 * Returns the keyword in the input line, if it exists
	 * 
	 * @param line
	 * 	The String to scan
	 * 
	 * @return
	 * 	The keyword that was found in the line
	 */
	public static String getKeyword(int index, String line) {
		String[] keywords = {"def","for ","while","if","else","elif"};
		
		for(int i=0; i<6; i++) {
			if(line.substring(index, index+keywords[i].length()).equals(keywords[i])) {
				if(keywords[i].equals("for ")) {
					return "for";
				}
				
				return keywords[i];
			}
		}
		
		return "";
	}
	
	/**
	 * Returns the number of times that token occurs in string
	 * 
	 * @param string
	 * 	The String to scan
	 * 
	 * @param token
	 * 	The substring to search for
	 * 
	 * @return
	 * 	The number of times that token occurs
	 */
	public static int count(String string, String token) {
		int count = 0;
		
		for(int i=0; i+token.length()<string.length(); i++) {			
			if(string.substring(i,i+token.length()).equals(token)) {
				count++;
			}
		}
		
		return count;
	}
	
	/**
	 * Takes in a full file path and returns just the file name
	 * 
	 * @param file
	 * 	The String to scan
	 * 
	 * @return
	 * 	The file name without the extension or path
	 */
	public static String getFileName(String file) {
		int index;
		for(index=file.length()-1; index>=0; index--) {
			if(file.charAt(index) == 92) {
				break;
			}
		}
		
		return file.substring(index+1,file.substring(index+1,file.length()).indexOf("."));
	}
}
