/**
* The <code>CodeBlock</code> class stores information about each block of code to calculate the complexity.
*    
* @author Nathan Wood
*    e-mail: nathan.wood@stonybrook.edu
*    Stony Brook ID: 109876625
* <dt><b>Assignment:</b><dd>
*    Homework #3 for CSE 214, Fall 2015
* <dt><b>Date:</b><dd>
*    October 6th, 2015
**/

public class CodeBlock {
	// ATTRIBUTES
	
	// Note: could not find a logical way to use these static variables
	public static final String[] BLOCK_TYPES = {"def","for ","while","if","else","elif"};
	public static final int DEF = 0, FOR = 1, WHILE = 2, IF = 3, ELIF = 4, ELSE = 5;
	
	private String name = "1", loopVariable = null;
	private Complexity blockComplexity, highestSubComplexity = new Complexity(0,0);
	
	// CONSTRUCTORS
	
	/**
	 * Default constructor for a CodeBlock object
	 * 
	 * <dt><b>Postcondition:</b><dd>
	 * 	This CodeBlock will have been initialized with the default values
	 */
	public CodeBlock() {
	}
	
	/**
	 * Secondary constructor for a CodeBlock object
	 * 
	 * @param blockComplexity
	 * 	The initial blockComplexity for this CodeBlock
	 * 
	 * @param name
	 * 	The name of this CodeBlock
	 * 
	 * <dt><b>Postcondition:</b><dd>
	 * 	This CodeBlock will have been initialized
	 */
	public CodeBlock(Complexity blockComplexity, String name) {
		this.blockComplexity = blockComplexity;
		this.name = name;
	}
	
	// ACCESSORS

	/**
	 * Returns the name of this CodeBlock
	 * 
	 * @return
	 * 	The name of this CodeBlock
	 */
	public String getName() {
		return name;
	}

	/**
	 * Returns the loopVariable for this CodeBlock, it will always be null unless it was a while loop
	 * 
	 * @return
	 * 	The loopVariable for this CodeBlock
	 */
	public String getLoopVariable() {
		return loopVariable;
	}

	/**
	 * Returns the blockComplexity of this CodeBlock
	 * 
	 * @return
	 * 	The blockComplexity of this CodeBlock
	 */
	public Complexity getBlockComplexity() {
		return blockComplexity;
	}

	/**
	 * Returns the highestSubComplexity of this CodeBlock
	 * 
	 * @return
	 * 	The highestSubComplexity of this CodeBlock
	 */
	public Complexity getHighestSubComplexity() {
		return highestSubComplexity;
	}
	
	// MUTATORS

	/**
	 * Sets the name of this CodeBlock
	 * 
	 * @param name
	 * 	The new name to set for this CodeBlock
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Sets the loopVariable of this CodeBlock
	 * 
	 * @param loopVariable
	 * 	The new loopVariable to set for this CodeBlock
	 */
	public void setLoopVariable(String loopVariable) {
		this.loopVariable = loopVariable;
	}

	/**
	 * Sets the blockComplexity of this CodeBlock
	 * 
	 * @param blockComplexity
	 * 	The new blockComplexity to set for this CodeBlock
	 */
	public void setBlockComplexity(Complexity blockComplexity) {
		this.blockComplexity = blockComplexity;
	}

	/**
	 * Sets the highestSubComplexity of this CodeBlock
	 * 
	 * @param highestSubComplexity
	 * 	The new highestSubComplexity to set for this CodeBlock
	 */
	public void setHighestSubComplexity(Complexity highestSubComplexity) {
		this.highestSubComplexity = highestSubComplexity;
	}

	/**
	 * Returns a String representation of this CodeBlock object
	 * 
	 * @return
	 * 	A formatted String containing the name and complexities of this Codeblock
	 */
	public String toString() {		
		return String.format("\t    %-20s%-30s%s\n", "BLOCK " + name + ":", "block complexity = " + blockComplexity.toString(),
				"highest sub-complexity = " + highestSubComplexity.toString());
	}
}
