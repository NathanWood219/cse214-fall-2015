/**
* A <code>NotADirectoryException</code> will be thrown if the selected DirectoryNode is not a directory
* 	when trying to add to this directory or change to this directory
* 
* @author Nathan Wood
*    e-mail: nathan.wood@stonybrook.edu
*    Stony Brook ID: 109876625
 * <dt><b>Assignment:</b><dd>
 *    Homework #5 for CSE 214, Fall 2015
 * <dt><b>Date:</b><dd>
 *    November 3rd, 2015
 **/
public class NotADirectoryException extends Exception {

	private static final long serialVersionUID = 6058979793447684821L;

	/**
     * Default constructor for a <CODE>NotADirectoryException</CODE> that
     * passes a default string to the <CODE>Exception</CODE> class constructor.
     *
     * <dt><b>Postcondition:</b><dd>
     *    The object is created and contains the default message.
     */
	public NotADirectoryException() {
		// Default message
		super("Error: parameter was not a directory.");
	}
	
    /**
     * Second constructor for the <CODE>NotADirectoryException</CODE> that
     * passes a provided string to the <CODE>Exception</CODE> class constructor.
     *
     * @param message
     *    the message that the object is to contain
     * <dt><b>Postcondition:</b><dd>
     *    The object is created and contains the provided message.
     */
    public NotADirectoryException(String message) {   
    	// Custom message
        super(message);
    }
}
