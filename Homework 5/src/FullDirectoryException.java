/**
* A <code>FullDirectoryException</code> will be thrown if the directory contains the maximum number of nodes.
* 
* @author Nathan Wood
*    e-mail: nathan.wood@stonybrook.edu
*    Stony Brook ID: 109876625
 * <dt><b>Assignment:</b><dd>
 *    Homework #5 for CSE 214, Fall 2015
 * <dt><b>Date:</b><dd>
 *    November 3rd, 2015
 **/
public class FullDirectoryException extends Exception {

	private static final long serialVersionUID = 7910150037208189791L;

	/**
     * Default constructor for a <CODE>FullDirectoryException</CODE> that
     * passes a default string to the <CODE>Exception</CODE> class constructor.
     *
     * <dt><b>Postcondition:</b><dd>
     *    The object is created and contains the default message.
     */
	public FullDirectoryException() {
		// Default message
		super("Error: this directory already contains the maximum number of nodes.");
	}
	
    /**
     * Second constructor for the <CODE>FullDirectoryException</CODE> that
     * passes a provided string to the <CODE>Exception</CODE> class constructor.
     *
     * @param message
     *    the message that the object is to contain
     * <dt><b>Postcondition:</b><dd>
     *    The object is created and contains the provided message.
     */
    public FullDirectoryException(String message) {   
    	// Custom message
        super(message);
    }
}
