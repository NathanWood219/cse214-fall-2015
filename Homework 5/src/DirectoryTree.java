/**
* The <code>DirectoryTree</code> class contains a tree of <code>DirectoryNode</code> objects.
*    
* @author Nathan Wood
*    e-mail: nathan.wood@stonybrook.edu
*    Stony Brook ID: 109876625
* <dt><b>Assignment:</b><dd>
*    Homework #5 for CSE 214, Fall 2015
* <dt><b>Date:</b><dd>
*    November 3rd, 2015
**/

public class DirectoryTree {
	// ATTRIBUTES
	private DirectoryNode root = new DirectoryNode(null, "root", false);
	private DirectoryNode cursor = root;

	// CONSTRUCTORS

	/**
	 * Default constructor for a DirectoryTree object
	 * 
	 * <dt><b>Postcondition:</b><dd>
	 * 	This DirectoryTree object will have been initialized with all the default attributes
	 */
	public DirectoryTree() {
	}
	
	// ACCESSORS
	
	/**
	 * Returns the DirectoryNode that cursor is pointed at
	 * 
	 * @return
	 * 	Returns cursor's reference
	 */
	public DirectoryNode getCursor() {
		return cursor;
	}
	
	// METHODS
	
	/**
	 * Resets the cursor back to the root
	 * 
	 * <dt><b>Postcondition:</b><dd>
	 * 	The cursor will now point at the root
	 */
	public void resetCursor() {
		this.cursor = root;
	}
	
	/**
	 * [EXTRA CREDIT] Recursively searches (as opposed to searching just the direct children)
	 * 		the entire tree under cursor for the directory with the input name and changed to it
	 * 
	 * @param name
	 * 	The name of the directory to change to
	 * 
	 * @throws NotADirectoryException
	 * 	Indicates that either the directory does not exist or it is a file
	 * 
	 * <dt><b>Postcondition:</b><dd>
	 * 	The cursor will now point to the DirectoryNode with the input name
	 */
	public void changeDirectory(String name) throws NotADirectoryException {
		DirectoryNode node = root.findChild(name);
		
		if(node == null) {
			System.out.println("ERROR: No such directory named '" + name + "'.");
			throw new NotADirectoryException();
		}
		
		if(node.isFile()) {
			System.out.println("ERROR: Cannot change directory into a file.");
			throw new NotADirectoryException();
		}
		
		cursor = node;
	}
	
	/**
	 * [EXTRA CREDIT] Recursively searches the entire tree under cursor for the directory
	 * 		with the input file path and changes to it if it exists
	 * 
	 * @param filePath
	 * 	The file path of the directory to change to
	 * 
	 * @throws NotADirectoryException
	 * 	Indicates that either the directory does not exist or it is a file
	 * 
	 * <dt><b>Postcondition:</b><dd>
	 * 	The cursor will now point to the DirectoryNode with the input file path
	 */
	public void changeDirectoryFilePath(String filePath) throws NotADirectoryException {
		DirectoryNode node = cursor.directoryFromFilePath("", filePath);	
		
		if(node == null) {
			System.out.println("ERROR: No such directory named '" + getNameFromFilePath(filePath) + "'.");
			throw new NotADirectoryException();
		}
		
		if(node.isFile()) {
			System.out.println("ERROR: Cannot change directory into a file.");
			throw new NotADirectoryException();
		}
		
		cursor = node;
	}
	
	/**
	 * Returns the file path of the directory at the cursor
	 * 
	 * @return
	 * 	The String file path that points to the cursor
	 */
	public String presentWorkingDirectory() {		
		return cursor.filePath();
	}
	
	/**
	 * Returns a formatted String containing the direct children at cursor
	 * 
	 * @return
	 * 	A String that contains each direct child of cursor
	 */
	public String listDirectory() {
		String output = "";
		
		for(int i=0; i<cursor.size(); i++) {
			output += cursor.getChild(i).getName() + " ";
		}
		
		return output.substring(0, output.length()-1);
	}
	
	/**
	 * Prints the entire directory beneath cursor
	 * 
	 * <dt><b>Postcondition:</b><dd>
	 * 	All of the DirectoryNodes under cursor will have been printed out 
	 */
	public void printDirectoryTree() {
		cursor.printPreorder(0);
	}
	
	/**
	 * Creates a new DirectoryNode at the cursor with the input name that is a directory
	 * 
	 * @param name
	 * 	The name of the new DirectoryNode
	 * 
	 * @throws IllegalArgumentException
	 * 	Indicates that the input name is not valid (contains white spaces)
	 * 
	 * @throws FullDirectoryException
	 * 	Indicates that there is no more room in the DirectoryNode at cursor
	 * 
	 * <dt><b>Postcondition:</b><dd>
	 * 	The cursor will contain a new child with the input name that is a directory
	 */
	public void makeDirectory(String name) throws IllegalArgumentException, FullDirectoryException {
		if(!isValidName(name)) {
			throw new IllegalArgumentException();
		}
		
		if(cursor.isFull()) {
			throw new FullDirectoryException();
		}
		
		try {
			cursor.addChild(new DirectoryNode(cursor, name, false));
		} catch(NotADirectoryException e) {
			System.out.println("ERROR: Parameter was not a directory.");
		}
	}
	
	/**
	 * Creates a new DirectoryNode at the cursor with the input name that is a file
	 * 
	 * @param name
	 * 	The name of the new DirectoryNode
	 * 
	 * @throws IllegalArgumentException
	 * 	Indicates that the input name is not valid (contains white spaces)
	 * 
	 * @throws FullDirectoryException
	 * 	Indicates that there is no more room in the DirectoryNode at cursor
	 * 
	 * <dt><b>Postcondition:</b><dd>
	 * 	The cursor will contain a new child with the input name that is a file
	 */
	public void makeFile(String name) throws IllegalArgumentException, FullDirectoryException {
		if(!isValidName(name)) {
			throw new IllegalArgumentException();
		}
		
		if(cursor.isFull()) {
			throw new FullDirectoryException();
		}
		
		try {
			cursor.addChild(new DirectoryNode(cursor, name, true));
		} catch(NotADirectoryException e) {
			System.out.println("ERROR: Parameter was not a directory.");
		}
	}
	
	/**
	 * [EXTRA CREDIT] Moves a file from src to dst if both exist and dst is a directory
	 * 
	 * @param src
	 * 	The file path of the file to move
	 * 
	 * @param dst
	 * 	The file path of the directory to move to
	 * 
	 * @throws NotADirectoryException
	 * 	Indicates that destination is not a directory or that the source or destination do not exist
	 * 
	 * @throws FullDirectoryException
	 * 	Indicates that the destination directory is full
	 * 
	 * <dt><b>Postcondition:</b><dd>
	 * 	The destination file path will now contain the entire directory located at the source file path
	 */
	public void moveFile(String src, String dst) throws FullDirectoryException, NotADirectoryException {
		DirectoryNode newParent = root.directoryFromFilePath("root", dst);
		DirectoryNode movedChild = root.directoryFromFilePath("root", src);
		
		// Even though there is a try/catch in this method, handle both exceptions beforehand
		// to prevent losing a child (removing a child and not being able to add it back)
		
		if(newParent == null) {
			System.out.println("ERROR: No such directory named '" + getNameFromFilePath(dst) + "'.");
			throw new NotADirectoryException();
		}

		if(newParent.isFile()) {
			System.out.println("ERROR: Cannot move to a file.");
			throw new NotADirectoryException();
		}
		
		if(movedChild == null) {
			System.out.println("ERROR: No such directory or file named '" + getNameFromFilePath(src) + "'.");
			throw new NotADirectoryException();
		}
		
		if(newParent.isFull()) {
			throw new FullDirectoryException();
		}
		
		try {
			movedChild.getParent().removeChild(movedChild);
			newParent.addChild(movedChild);
			movedChild.setParent(newParent);
		} catch(NotADirectoryException e) {
			System.out.println("ERROR: Cannot move to a file.");
		} catch(EmptyDirectoryException e) {
			System.out.println("ERROR: Source directory is empty.");
		}
	}
	
	/**
	 * [EXTRA CREDIT] Calls the recursive function that searches for all occurrences of files
	 * 		with the input name and prints them
	 * 
	 * @param name
	 * 	The name to search for
	 * 
	 * <dt><b>Postcondition:</b><dd>
	 * 	The file paths of each match will be printed, otherwise if it is not found then an error will print
	 */
	public void findFilePath(String name) {
		if(!root.printFilePaths(name)) {
			System.out.println("ERROR: No such file exists.");
		}
	}
	
	/**
	 * Checks the input name and returns true if it is allowed
	 * 
	 * @param name
	 * 	The name to parse
	 * 
	 * @return
	 * 	Returns true if the input name does not contain any spaces or back slashes
	 */
	public static boolean isValidName(String name) {		
		for(int i=0; i<name.length(); i++) {
			if(name.charAt(i) == 32 || name.charAt(i) == 47) {
				return false;
			}
		}
		
		return true;
	}
	
	/**
	 * Takes a file path as a parameter and returns the name
	 * 
	 * @param filePath
	 * 	The file path to parse
	 * 
	 * @return
	 * 	The name of the file at the end of the file path
	 */
	public static String getNameFromFilePath(String filePath) {
		while(filePath.contains("/")) {
			if(filePath.indexOf("/") == filePath.length()-1) {
				return filePath.substring(0, filePath.length()-1);
			}
			
			filePath = filePath.substring(filePath.indexOf("/")+1);
		}
		
		return filePath;
	}
}
