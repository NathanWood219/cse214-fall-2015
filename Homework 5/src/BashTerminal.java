/**
* The <code>BashTerminal</code> class allows the user to enter commands to create and
* 	navigate through a DirectoryTree.
*    
* @author Nathan Wood
*    e-mail: nathan.wood@stonybrook.edu
*    Stony Brook ID: 109876625
* <dt><b>Assignment:</b><dd>
*    Homework #5 for CSE 214, Fall 2015
* <dt><b>Date:</b><dd>
*    November 3rd, 2015
**/

import java.util.Scanner;

public class BashTerminal {
	public static void main(String args[] ) {
		// Initalize variables
		Scanner input = new Scanner(System.in);
		DirectoryTree tree = new DirectoryTree();
		String userInput;
		
		System.out.println("Starting bash terminal.");
		
		// Continue to loop until the user enters "exit"
		while(true) {
			// Print the start of each command line and receive user input
			System.out.print("[ncwood@host]: $ ");
			userInput = input.nextLine();
			
			if(checkInput(userInput, "pwd")) {												// print the present working directory (file path)
				
				System.out.println(tree.presentWorkingDirectory());
				
			} else if(checkInput(userInput, "ls -R")) {										// print the preorder traversal of the cursor subtree
				
				System.out.println();
				tree.printDirectoryTree();
				System.out.println();
				
			} else if(checkInput(userInput, "ls")) {										// list the name of all the direct child directories or files of cursor
				
				System.out.println(tree.listDirectory());
				
			} else if(checkInput(userInput, "cd /")) {										// moves the cursor to the root
				
				tree.resetCursor();
				
			} else if(checkInput(userInput, "cd ..")) {										// [EXTRA CREDIT] moves the cursor to the parent node if not at root
				
				try {
					DirectoryNode parent = tree.getCursor().getParent();
					
					if(parent == null) {
						System.out.println("ERROR: Already at root directory.");
					} else {
						tree.changeDirectory(parent.getName());
					}
				} catch(NotADirectoryException e) {
					System.out.println("ERROR: Parameter was not a directory.");
				}
				
			} else if(checkInput(userInput, "cd ")) {										// additional cursor movement commands
				
				try {
					if(userInput.substring(3).contains("/")) {								// [EXTRA CREDIT] moving to a directory using a file path
						tree.changeDirectoryFilePath(userInput.substring(3));
					} else {																// moving to a directory using the name
						tree.changeDirectory(userInput.substring(3));
					}
				} catch(NotADirectoryException e) {
					// Do nothing -- this is printed in the method itself to handle the two special cases
				}
				
			} else if(checkInput(userInput, "mkdir ")) {									// makes a new directory with input name under cursor if possible
				
				try {
					tree.makeDirectory(userInput.substring(6));
				} catch(IllegalArgumentException e) {
					System.out.println("ERROR: Invalid name.");
				} catch(FullDirectoryException e) {
					System.out.println("ERROR: Present directory is full.");
				}
				
			} else if(checkInput(userInput, "touch ")) {									// makes a new file with input name under cursor if possible
				
				try {
					tree.makeFile(userInput.substring(6));
				} catch(IllegalArgumentException e) {
					System.out.println("ERROR: Invalid name.");
				} catch(FullDirectoryException e) {
					System.out.println("ERROR: Present directory is full.");
				}
				
			} else if(checkInput(userInput, "find ")) {										// [EXTRA CREDIT] finds the nodes in the tree with input name and prints path
							
				tree.findFilePath(userInput.substring(5));
				
			} else if(checkInput(userInput, "mv ")) {										// [EXTRA CREDIT] moves a file or directory specified by src to dst
				
				int dstStart = -1;
				String filePaths = userInput.substring(3);
				
				for(int i=0; i<filePaths.length(); i++) {
					if(filePaths.charAt(i) == 32) {
						dstStart = i+1;
					}
				}
				
				if(dstStart == -1) {
					System.out.println("ERROR: Requires two file paths.");
				} else {
					try {						
						tree.moveFile(filePaths.substring(0,dstStart-1), filePaths.substring(dstStart));
					} catch (FullDirectoryException e) {
						System.out.println("ERROR: Destination directory is full.");
					} catch(NotADirectoryException e) {
						// Handled in the function due to specific cases
					}
				}
				
			} else if(checkInput(userInput, "exit")){										// terminates the program
				
				break;
				
			} else {																		// user did not enter a valid command, request input again at start of while loop
				
				System.out.println("ERROR: Invalid command.");
				
			}
		}
		
		// This is only reached if the while loop is broken by entering the "exit" command
		System.out.println("Program terminating successfully...");
		input.close();
	}
	
	/**
	 * Checks if the user input contains a specific command
	 * 
	 * @param userInput
	 * 	The input of the user to check
	 * 
	 * @param command
	 * 	The command to check for
	 * 
	 * @return
	 * 	Returns true if the command is found at the start of the user input
	 */
	public static boolean checkInput(String userInput, String command) {
		if(command.length() > userInput.length()) {
			return false;
		}
		
		if(userInput.substring(0,command.length()).equals(command)) {
			return true;
		}
		
		return false;
	}
}
