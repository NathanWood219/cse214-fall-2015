/**
* A <code>EmptyDirectoryException</code> will be thrown if the directory contains no children
* 	when the removeChild() function is called.
* 
* @author Nathan Wood
*    e-mail: nathan.wood@stonybrook.edu
*    Stony Brook ID: 109876625
 * <dt><b>Assignment:</b><dd>
 *    Homework #5 for CSE 214, Fall 2015
 * <dt><b>Date:</b><dd>
 *    November 3rd, 2015
 **/
public class EmptyDirectoryException extends Exception {

	private static final long serialVersionUID = -3758678255608940699L;

	/**
     * Default constructor for a <CODE>EmptyDirectoryException</CODE> that
     * passes a default string to the <CODE>Exception</CODE> class constructor.
     *
     * <dt><b>Postcondition:</b><dd>
     *    The object is created and contains the default message.
     */
	public EmptyDirectoryException() {
		// Default message
		super("Error: this directory is empty.");
	}
	
    /**
     * Second constructor for the <CODE>EmptyDirectoryException</CODE> that
     * passes a provided string to the <CODE>Exception</CODE> class constructor.
     *
     * @param message
     *    the message that the object is to contain
     * <dt><b>Postcondition:</b><dd>
     *    The object is created and contains the provided message.
     */
    public EmptyDirectoryException(String message) {   
    	// Custom message
        super(message);
    }
}
