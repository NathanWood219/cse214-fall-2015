/**
* The <code>DirectoryNode</code> class represents a node in the file tree contained by <code>DirectoryTree</code>.
*    
* @author Nathan Wood
*    e-mail: nathan.wood@stonybrook.edu
*    Stony Brook ID: 109876625
* <dt><b>Assignment:</b><dd>
*    Homework #5 for CSE 214, Fall 2015
* <dt><b>Date:</b><dd>
*    November 3rd, 2015
**/

public class DirectoryNode {
	// ATTRIBUTES
	private String name = "";
	private boolean isFile = false;
	private DirectoryNode parent; 										// [EXTRA CREDIT] used for a more efficient traversal backwards through the tree
	private DirectoryNode[] children = new DirectoryNode[10];			// [EXTRA CREDIT] an array of DirectoryNodes to hold up to 10 children
	
	// CONSTRUCTORS

	/**
	 * Default constructor for a DirectoryNode object
	 * 
	 * <dt><b>Postcondition:</b><dd>
	 * 	This DirectoryNode object will have been initialized with no name and all the default attributes
	 */
	public DirectoryNode() {
	}

	/**
	 * Secondary constructor for a DirectoryNode object with parameters
	 * 
	 * @param parent
	 * 	The parent of this new DirectoryNode
	 * 
	 * @param name
	 * 	The String name of this DirectoryNode
	 * 
	 * @param isFile
	 * 	Whether this is a file or directory
	 * 
	 * <dt><b>Postcondition:</b><dd>
	 * 	This DirectoryNode object will have been initialized with the default length and specified parameters
	 */
	public DirectoryNode(DirectoryNode parent, String name, boolean isFile) {
		this.parent = parent;
		this.name = name;
		this.isFile = isFile;
	}
	
	// ACCESSORS
	
	/**
	 * Returns the parent DirectoryNode for DirectoryNode
	 * 
	 * @return
	 * 	The parent of this object
	 */
	public DirectoryNode getParent() {
		return parent;
	}
	
	/**
	 * Returns the name of this DirectoryNode
	 * 
	 * @return
	 * 	The String name of this object
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Returns true if this DirectoryNode is a file
	 * 
	 * @return
	 * 	A boolean containing whether this is a file or not
	 */
	public boolean isFile() {
		return isFile;
	}
	
	/**
	 * Returns true if this DirectoryNode is empty
	 * 
	 * @return
	 * 	A boolean containing whether this is empty or not
	 */
	public boolean isEmpty() {
		return children[0] == null;
	}
	
	/**
	 * Returns true if this DirectoryNode is full
	 * 
	 * @return
	 * 	A boolean containing whether this is full or not
	 */
	public boolean isFull() {
		return !(children[children.length-1] == null);
	}
	
	/**
	 * Returns the child of this DirectoryNode at index
	 * 
	 * @param childIndex
	 * 	The position of the child to return
	 * 
	 * @throws IllegalArgumentException
	 * 	Indicates the index was out of the accepted range (0 <= index <= 9)
	 * 
	 * @return
	 * 	The child at position childIndex, can be null
	 */
	public DirectoryNode getChild(int childIndex) throws IllegalArgumentException {
		if(childIndex < 0 || childIndex > 9) {
			throw new IllegalArgumentException();
		}
		
		return children[childIndex];
	}
	
	/**
	 * Returns the number of children in this DirectoryNode
	 * 
	 * @return
	 * 	The number of children
	 */
	public int size() {
		for(int i=0; i<children.length-1; i++) {
			if(children[i] == null) {
				return i;
			}
		}
		
		return children.length;
	}
	
	// MUTATORS
	
	/**
	 * Sets the parent of this object to the input DirectoryNode
	 * 
	 * @param parent
	 * 	The new parent of this object
	 * 
	 * <dt><b>Postcondition:</b><dd>
	 * 	The parent reference of this object will have changed
	 */
	public void setParent(DirectoryNode parent) {
		this.parent = parent;
	}
	
	/**
	 * Sets the name of this object to the input String
	 * 
	 * @param name
	 * 	The new name of this object
	 * 
	 * <dt><b>Postcondition:</b><dd>
	 * 	The name will now be the same as the input
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	// METHODS
	
	/**
	 * Adds the input child to this DirectoryNode
	 * 
	 * @param newChild
	 * 	The child to add to this object
	 * 
	 * @throws NotADirectoryException
	 * 	Indicates that this is a file
	 * 
	 * @throws FullDirectoryException
	 * 	Indicates that this directory is at full capacity
	 * 
	 * <dt><b>Postcondition:</b><dd>
	 * 	This directory will contain another child reference
	 */
	public void addChild(DirectoryNode newChild) throws NotADirectoryException, FullDirectoryException {
		if(isFile) {
			throw new NotADirectoryException();
		}
		
		if(this.isFull()) {
			throw new FullDirectoryException();
		}
		
		for(int i=0; i<children.length; i++) {
			if(children[i] == null) {
				children[i] = newChild;
				break;
			}
		}
	}
	
	/**
	 * [EXTRA CREDIT] Finds and removes the input child from this DirectoryNode, used for moving a file
	 * 
	 * @param child
	 * 	The child to remove from this object
	 * 
	 * @throws NotADirectoryException
	 * 	Indicates that this is a file
	 * 
	 * @throws EmptyDirectoryException
	 * 	Indicates that this directory has no children
	 * 
	 * <dt><b>Postcondition:</b><dd>
	 * 	This directory will no longer contain the child reference
	 */
	public void removeChild(DirectoryNode child) throws NotADirectoryException, EmptyDirectoryException {
		if(isFile) {
			throw new NotADirectoryException();
		}
		
		if(this.isEmpty()) {
			throw new EmptyDirectoryException();
		}
		
		boolean found = false;
		int size = this.size();
		
		for(int i=0; i<size; i++) {
			if(children[i] == child) {
				found = true;
			}
			
			if(found && i<children.length-1) {
				children[i] = children[i+1];
			}
		}
		
		children[size--] = null;
	}
	
	/**
	 * Recursively finds a descendant of this DirectoryNode based on the name and returns the node once found
	 * 
	 * @param name
	 * 	The name to search for
	 * 
	 * @return
	 * 	Returns the child if found, or null if it does not exist in the directory
	 */
	public DirectoryNode findChild(String name) {
		DirectoryNode temp;
		
		if(this.getName().equals(name)) {
			return this;
		}
		
		for(int i=0; i<children.length; i++) {
			if(children[i] != null) {
				temp = children[i].findChild(name);
				
				if(temp != null) {
					return temp;
				}
			}
		}
		
		return null;
	}
	
	/**
	 * Recursively prints all the children that share the input name
	 * 
	 * @param name
	 * 	The name to search for
	 * 
	 * @return
	 * 	Returns true as long as it printed at least one child
	 * 
	 * <dt><b>Postcondition:</b><dd>
	 * 	All descendants that have the input name will have their file paths printed
	 */
	public boolean printFilePaths(String name) {
		boolean printed = false;
		
		if(this.getName().equals(name)) {
			System.out.println(this.filePath());
			printed = true;
		}
		
		for(int i=0; i<children.length; i++) {
			if(children[i] != null) {
				printed = children[i].printFilePaths(name);
			}
		}
		
		return printed;
	}
	
	/**
	 * Traverses from this object up to the root to find and return the full file path
	 * 
	 * @return
	 * 	The file path from this DirectoryNode back to the highest point
	 */
	public String filePath() {
		String filePath = "";
		DirectoryNode temp = this;
		
		while(temp != null) {
			filePath = "/" + temp.getName() + filePath;
			temp = temp.getParent();
		}
		
		return filePath.substring(1);
	}
	
	/**
	 * Recursively searches this directory for the DirectoryNode at the input file path
	 * 
	 * @param currentFilePath
	 * 	The file path of the current position in the directory
	 * 
	 * @param filePath
	 * 	The file path that is being searched for
	 * 
	 * @return
	 * 	Returns the DirectoryNode that is at the input file path, null if it does not exist
	 */
	public DirectoryNode directoryFromFilePath(String currentFilePath, String filePath) {
		DirectoryNode node;
		
		if(currentFilePath.equals(filePath)) {
			return this;
		}
		
		for(int i=0; i<children.length; i++) {
			if(children[i] != null) {
				node = children[i].directoryFromFilePath(currentFilePath + (currentFilePath.isEmpty() ? "" : "/")
						+ children[i].getName(), filePath);
				
				if(node != null) {
					return node;
				}
			}
		}
		
		return null;
	}
	
	/**
	 * Recursively prints the preorder traversal of this directory with proper tabbing
	 * 
	 * @param tabs
	 * 	The number of times to tab the files in this directory, also the current depth
	 * 
	 * <dt><b>Postcondition:</b><dd>
	 * 	The entire preorder representation of the nodes under this directory will have been printed
	 */
	public void printPreorder(int tabs) {
		String output = "";
		
		for(int i=0; i<tabs; i++) {
			output += "    ";
		}
		
		System.out.println(output + (isFile ? "- " : "|- ") + name);
		
		for(int i=0; i<children.length; i++) {
			if(children[i] != null) {
				children[i].printPreorder(tabs + 1);
			}
		}
	}
}
