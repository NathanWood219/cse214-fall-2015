/**
* The <code>SearchEngine</code> class uses the WebGraph data structure to work with WebPages.
* 
* @author Nathan Wood
*    e-mail: nathan.wood@stonybrook.edu
*    Stony Brook ID: 109876625
 * <dt><b>Assignment:</b><dd>
 *    Homework #7 for CSE 214, Fall 2015
 * <dt><b>Date:</b><dd>
 *    December 1st, 2015
 **/

import java.util.Scanner;

public class SearchEngine {
	public static final String PAGES_FILE = "pages.txt";
	public static final String LINKS_FILE = "links.txt";
	
	public static void main(String args[] ) {
		Scanner input = new Scanner(System.in);
		
		System.out.println("Loading WebGraph data...");
		
		// Attempt to build the WebGraph from the files in the project folder
		
		WebGraph web = new WebGraph();
		
		try {
			web = WebGraph.buildFromFiles(PAGES_FILE, LINKS_FILE);
			web.updatePageRanks();
			
			System.out.println("Success!");
		} catch(IllegalArgumentException e) {
			System.out.println("ERROR: Could not find the files.");
		}
		
		String userInput = "";
		boolean printMenu = true;
		boolean getInput = true;
		
		// Only can be broken when the user selects the Quit option
		while(true) {
			
			// Only print the menu if there was no incorrect input
			if(printMenu) {
				System.out.println("\nMenu:\n\t(AP) - Add a new page to the graph.\n\t(RP) - Remove a page from the graph.\n\t"
						+ "(AL) - Add a link between pages in the graph.\n\t(RL) - Remove a link between pages in the graph.\n\t"
						+ "(P)  - Print the graph.\n\t(S)  - Search for pages with a keyword.\n\t(Q)  - Quit\n");
			}
			
			// Reset printMenu
			printMenu = true;
			
			// Only get user input if there was no incorrect input in the printing submenu
			if(getInput) {
				System.out.print("Please select an option: ");
				userInput = input.nextLine().toUpperCase();
			}
			
			// Reset getInput
			getInput = true;
			
			// Check user input for the specific commands
			if(userInput.equals("AP")) {													// (AP) - Add a new page to the graph
				
				String source = "UNKNOWN";
				
				try {
					System.out.print("Enter a URL: ");
					source = input.nextLine();
					
					System.out.print("Enter keywords (space-seperated): ");
					String[] keywords = input.nextLine().split(" ");
					
					web.addPage(source, keywords);
					System.out.println("\n" + source + " successfully added to the WebGraph!");
					
				} catch(IllegalArgumentException e) {
					System.out.println("\nERROR: Could not add " + source + " to the WebGraph.");
				}
				
			} else if(userInput.equals("RP")) {												// (RP) - Remove a page from the graph
				
				userInput = "UNKNOWN";
				
				try {
					System.out.print("Enter a URL: ");
					userInput = input.nextLine();
					
					web.removePage(userInput);
					System.out.println("\n" + userInput + " has been removed from the graph!");
					
				} catch(IllegalArgumentException e) {
					System.out.println("\nERROR: Could not remove " + userInput + " from the WebGraph.");
				}
				
			} else if(userInput.equals("AL")) {												// (AL) - Add a link between pages in the graph
				
				String source = "UNKNOWN", dest = "UNKNOWN";
				
				try {
					System.out.print("Enter a source URL: ");
					source = input.nextLine();
					
					System.out.print("Enter a destination URL: ");
					dest = input.nextLine();
					
					web.addLink(source,  dest);
					System.out.println("\nLink successfully added from " + source + " to " + dest + "!");
					
				} catch(IllegalArgumentException e) {
					System.out.println("\nERROR: Could not add a link from " + source + " to " + dest + ".");
				}
				
			} else if(userInput.equals("RL")) {												// (RL) - Remove a link between pages in the graph
				
				System.out.print("Enter a source URL: ");
				String source = input.nextLine();
				
				System.out.print("Enter a destination URL: ");
				String dest = input.nextLine();
				
				web.removeLink(source, dest);
				System.out.println("\nLink from " + source + " to " + dest + " successfully removed.");
				
			} else if(userInput.equals("P")) {												// (P) - Print the graph
				
				if(printMenu) {
					System.out.println("\n\t(I) - Sort based on index (ASC)\n\t(U) - Sort based on URL (ASC)\n"
							+ "\t(R) - Sort based on rank (DSC)\n\t(Q) - Quit (return to previous menu)");
				}
				
				System.out.print("\nPlease select an option: ");
				userInput = input.nextLine().toUpperCase();
				
				if(userInput.equals("I")) {													// (I) - Sort based on index (ASC)
					
					web.printTable(0);
					
				} else if(userInput.equals("U")) {											// (U) - Sort based on URL (ASC)
					
					web.printTable(1);
					
				} else if(userInput.equals("R")) {											// (R) - Sort based on rank (DSC)
					
					web.printTable(2);
					
				} else if(userInput.equals("Q")) {											// (Q) - Quit (return to previous menu)
					
					// do nothing, just loop back to the main menu
					
				} else {
					
					// Loops back to the printing submenu
					System.out.println("ERROR: Unknown command.");
					getInput = false;
					printMenu = false;
					userInput = "P";
					
				}
				
			} else if(userInput.equals("S")) {												// (S) - Search for pages with a keyword
				
				System.out.print("Search keyword: ");
				web.searchAndPrint(input.nextLine());
				
			} else if(userInput.equals("Q")) {												// (Q) - Quit
				
				break;
				
			} else {																		// Faulty input
				
				// Loops back to the get user input section
				System.out.println("ERROR: Unknown command.\n");
				printMenu = false;
				
			}
		}
		
		// This point is only reached if the user selects the Quit option
		System.out.println("\nGoodbye.");
		input.close();
	}
}
