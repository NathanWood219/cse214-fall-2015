/**
* The <code>WebPage</code> class contains the data fields and methods required to manipulate a custom web page.
* 
* @author Nathan Wood
*    e-mail: nathan.wood@stonybrook.edu
*    Stony Brook ID: 109876625
 * <dt><b>Assignment:</b><dd>
 *    Homework #7 for CSE 214, Fall 2015
 * <dt><b>Date:</b><dd>
 *    December 1st, 2015
 **/

public class WebPage implements Comparable<Object> {
	// ATTRIBUTES
	private String url;
	private int index, rank;
	private String[] keywords;
	
	// CONSTRUCTORS

	/**
	 * Constructor for a WebPage object
	 * 
	 * @param url
	 * 	The address of this WebPage
	 * 
	 * @param index
	 * 	The index of this WebPage
	 * 
	 * @param keywords
	 * 	A list of the keywords associated with this WebPage
	 * 
	 * <dt><b>Postcondition:</b><dd>
	 * 	This WebPage object will have been initialized with the specified url, index, and keywords
	 */
	public WebPage(String url, int index, String[] keywords) {
		this.url = url;
		this.index = index;
		this.keywords = keywords;
	}
	
	// ACCESSORS
	
	/**
	 * Returns the URL (address) of this WebPage
	 * 
	 * @return
	 * 	The String address of this WebPage
	 */
	public String getURL() {
		return url;
	}
	
	/**
	 * Returns the index of this WebPage
	 * 
	 * @return
	 * 	The index of this WebPage
	 */
	public int getIndex() {
		return index;
	}
	
	/**
	 * Returns the rank of this WebPage
	 * 
	 * @return
	 * 	The rank of this WebPage
	 */
	public int getRank() {
		return rank;
	}
	
	/**
	 * Returns the keywords of this WebPage
	 * 
	 * @return
	 * 	The keywords of this WebPage
	 */
	public String[] getKeywords() {
		return keywords;
	}
	
	// MUTATORS
	
	/**
	 * Sets the index of this WebPage to the argument
	 * 
	 * @param index
	 * 	The new index for this WebPage
	 * 
	 * <dt><b>Postcondition:</b><dd>
	 * 	The index will have changed to the argument
	 */
	public void setIndex(int index) {
		this.index = index;
	}
	
	/**
	 * Sets the rank of this WebPage to the argument
	 * 
	 * @param rank
	 * 	The new rank for this WebPage
	 * 
	 * <dt><b>Postcondition:</b><dd>
	 * 	The rank will have changed to the argument
	 */
	public void setRank(int rank) {
		this.rank = rank;
	}
	
	// METHODS
	
	/**
	 * Compares this WebPage to the input WebPage based on index
	 * 
	 * @param other
	 * 	The other WebPage to compare this one to
	 * 
	 * @return
	 * 	Returns an integer between -1 and 1 based on the comparison
	 */
	public int compareTo(Object other) {
		WebPage o = (WebPage)other;
		
		if(index > o.getIndex()) {
			return 1;
		} else if(index < o.getIndex()) {
			return -1;
		}
		return 0;
	}
	
	/**
	 * Searches the keywords in this WebPage for a specific keyword
	 * 
	 * @param keyword
	 * 	The keyword to search this WebPage for
	 * 
	 * @return
	 * 	Returns true if the keyword was found in this WebPage
	 */
	public boolean containsKeyword(String keyword) {
		for(int i=0; i<keywords.length; i++) {
			if(keywords[i].equals(keyword)) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Returns a String representation of this WebPage
	 * 
	 * @return
	 * 	Returns a formatted String representing this WebPage
	 */
	public String toString() {
		String keywordsString = "";
		
		for(int i=0; i<keywords.length; i++) {
			if(i == keywords.length - 1) {
				keywordsString += keywords[i];
			} else {
				keywordsString += keywords[i] + ", ";
			}
		}
		
		return String.format("  %-3d | %-18s |    %d    | *** | %-50s", index, url, rank, keywordsString);
	}
}
