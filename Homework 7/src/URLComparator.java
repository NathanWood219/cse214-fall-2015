/**
* The <code>URLComparator</code> class allows a WebGraph to be sorted based on URL.
* 
* @author Nathan Wood
*    e-mail: nathan.wood@stonybrook.edu
*    Stony Brook ID: 109876625
 * <dt><b>Assignment:</b><dd>
 *    Homework #7 for CSE 214, Fall 2015
 * <dt><b>Date:</b><dd>
 *    December 1st, 2015
 **/

import java.util.Comparator;

public class URLComparator implements Comparator<Object> {
	
	/**
	 * Compares two WebPage objects to each other based on URL
	 * 
	 * @param o1
	 * 	The first WebPage to compare
	 * 
	 * @param o2
	 * 	The second WebPage to compare
	 * 
	 * @return
	 * 	Returns an integer between -1 and 1 based on the comparison (ASC)
	 */
	public int compare(Object o1, Object o2) {
		WebPage webPage1 = (WebPage)o1;
		WebPage webPage2 = (WebPage)o2;
		
		return webPage1.getURL().compareTo(webPage2.getURL());
	}
}
