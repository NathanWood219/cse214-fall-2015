/**
* The <code>WebGraph</code> class contains a list of WebPages and methods to manipulate them like a directed graph.
* 
* @author Nathan Wood
*    e-mail: nathan.wood@stonybrook.edu
*    Stony Brook ID: 109876625
 * <dt><b>Assignment:</b><dd>
 *    Homework #7 for CSE 214, Fall 2015
 * <dt><b>Date:</b><dd>
 *    December 1st, 2015
 **/

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class WebGraph {
	// ATTRIBUTES
	public static final int MAX_PAGES = 40;
	private ArrayList<WebPage> pages = new ArrayList<WebPage>();
	private int[][] links = new int[MAX_PAGES][MAX_PAGES];
	
	// METHODS
	
	/**
	 * Attempts to create and return a new WebGraph by retrieving the information from an input file.
	 * 
	 * @param pagesFile
	 * 	The file that contains the new pages to construct
	 * 
	 * @param linksFile
	 * 	The file that contains the new links to construct
	 * 
	 * @return
	 * 	Returns a newly constructed WebGraph
	 */
	public static WebGraph buildFromFiles(String pagesFile, String linksFile) {
		
		try {
			
			WebGraph newWebGraph = new WebGraph();
			
			Scanner fileInput = new Scanner(new File(pagesFile));
			
			// Read in and store the pages
			while(fileInput.hasNextLine()) {
				String[] line = fileInput.nextLine().split(" ");
				String[] keywords = new String[1];
				int startIndex = 0;
				
				String url = "";
				
				for(int i=0; i<line.length; i++) {
					if(!line[i].isEmpty()) {
						if(url.isEmpty()) {
							url = line[i];
							keywords = new String[line.length - (i + 1)];
							startIndex = i+1;
						} else {
							keywords[i-startIndex] = line[i];
						}
					}
					
				}
				
				newWebGraph.addPage(url, keywords);
			}
			
			// Read in and store the links
			fileInput = new Scanner(new File(linksFile));
			
			while(fileInput.hasNextLine()) {
				String[] line = fileInput.nextLine().split(" ");
				
				for(int i=0; i<line.length-1; i++) {
					if(!line[i].isEmpty()) {
						newWebGraph.addLink(line[i], line[i+1]);
						break;
					}
				}
			}
			
			fileInput.close();
			return newWebGraph;
			
		} catch(FileNotFoundException|IllegalArgumentException e) {
			throw new IllegalArgumentException();
		}
	}
	
	/**
	 * Adds a new WebPage to this WebGraph if the URL is unique
	 * 
	 * @param url
	 * 	The URL of the new WebPage
	 * 
	 * @param keywords
	 * 	The keywords for the new WebPage
	 * 
	 * @throws IllegalArgumentException
	 * 	Thrown if the URL or keywords are null, or if the URL is not unique
	 * 
	 * <dt><b>Postcondition:</b><dd>
	 *  This WebGraph will now contain the new WebPage
	 */
	public void addPage(String url, String[] keywords) throws IllegalArgumentException {
		if(url == null || keywords == null || indexOfURL(url) >= 0) {
			throw new IllegalArgumentException();
		}
		
		pages.add(new WebPage(url, pages.size(), keywords));
		
		for(int i=0; i<pages.size(); i++) {
			links[pages.size()-1][i] = 0;
			links[i][pages.size()-1] = 0;
		}
	}
	
	/**
	 * Adds a link between two existing WebPages to this WebGraph
	 * 
	 * @param source
	 * 	The URL of the source WebPage
	 * 
	 * @param destination
	 * 	The URL of the destination WebPage
	 * 
	 * @throws IllegalArgumentException
	 * 	Thrown if the source or destination are null, or if either URL does not exist
	 * 
	 * <dt><b>Postcondition:</b><dd>
	 *  The source WebPage will now link to the destination WebPage
	 */
	public void addLink(String source, String destination) throws IllegalArgumentException {
		if(source == null || destination == null) {
			throw new IllegalArgumentException();
		}
		
		int sourceIndex = indexOfURL(source);
		int destinationIndex = indexOfURL(destination);
		
		if(sourceIndex == -1 || destinationIndex == -1) {
			throw new IllegalArgumentException();
		}
		
		links[sourceIndex][destinationIndex] = 1;
		
		this.updatePageRanks();
	}
	
	/**
	 * Removes a WebPage from this WebGraph and all of its links
	 * 
	 * @param url
	 * 	The URL of the WebPage to remove
	 * 
	 * @throws IllegalArgumentException
	 * 	Thrown if the URL is null or if the URL does not exist
	 * 
	 * <dt><b>Postcondition:</b><dd>
	 *  The WebPage will no longer be in this WebGraph and its links will have been removed
	 */
	public void removePage(String url) throws IllegalArgumentException {
		int pageIndex;
		
		if(url == null || (pageIndex = indexOfURL(url)) == -1) {
			throw new IllegalArgumentException();
		}
		
		for(int i=pageIndex+1; i<pages.size(); i++) {
			pages.get(i).setIndex(i-1);
		}
		
		for(int k=pageIndex; k<pages.size(); k++) {
			for(int j=0; j<=k; j++) {
				if(k==j) {
					links[k-1][k] = links[k][k+1];
					links[k][k-1] = links[k+1][k];
				} else {
					links[k][j] = links[k+1][j];
					links[j][k] = links[j][k+1];
				}
			}
		}
		
		pages.remove(pageIndex);
		
		this.updatePageRanks();
	}
	
	/**
	 * Removes a WebPage from this WebGraph and all of its links
	 * 
	 * @param source
	 * 	The URL of the source WebPage
	 * 
	 * @param destination
	 * 	The URL of the destination WebPage
	 * 
	 * <dt><b>Postcondition:</b><dd>
	 *  The link from the source to the destination will have been removed
	 */
	public void removeLink(String source, String destination) {
		int sourceIndex = indexOfURL(source);
		int destinationIndex = indexOfURL(destination);
		
		if(sourceIndex >= 0 || destinationIndex >= 0) {
			links[sourceIndex][destinationIndex] = 0;
		}
		
		this.updatePageRanks();
	}
	
	/**
	 * Updates all of the page ranks for each WebPage in this WebGraph
	 * 
	 * <dt><b>Postcondition:</b><dd>
	 *  All of the page ranks will now reflect the current links array
	 */
	public void updatePageRanks() {
		for(int k=0; k<pages.size(); k++) {
			int rank = 0;
			
			for(int i=0; i<pages.size(); i++) {
				rank += links[i][k];
			}
			
			pages.get(k).setRank(rank);
		}
	}
	
	/**
	 * Searches for a specific keyword and prints a formatted table of all the found WebPages
	 * 
	 * @param keyword
	 * 	The keyword to search for
	 * 
	 * <dt><b>Postcondition:</b><dd>
	 *  A sorted table will be printed containing all the WebPages that share the keyword
	 */
	public void searchAndPrint(String keyword) {
		ArrayList<WebPage> tempList = new ArrayList<WebPage>();
		
		for(int i=0; i<pages.size(); i++) {
			WebPage temp = pages.get(i);
			
			if(temp.containsKeyword(keyword)) {
				tempList.add(temp);
			}
		}
		
		if(tempList.isEmpty()) {
			System.out.println("\nNo search results found for the keyword " + keyword + ".");
			return;
		}
		
		System.out.println("\nRank   PageRank    URL");
		System.out.println("---------------------------------------------");
		
		Collections.sort(tempList, new RankComparator());
		
		for(int i=0; i<tempList.size(); i++) {
			WebPage temp = tempList.get(i);
			
			System.out.printf("  %-3d |    %d    | %-50s\n", i+1, temp.getRank(), temp.getURL());
		}
	}
	
	/**
	 * Prints this entire WebGraph in a formatted table, sorted based on the input
	 * 
	 * @param sortMethod
	 * 	Determines how the table will be sorted. 0 = by index, 1 = by URL, 2 = by rank
	 * 
	 * <dt><b>Postcondition:</b><dd>
	 *  A sorted table will be printed containing all the WebPages in this WebGraph
	 */
	public void printTable(int sortMethod) throws IllegalArgumentException {
		if(sortMethod < 0 || sortMethod > 2) {
			throw new IllegalArgumentException();
		}
		
		if(pages.isEmpty()) {
			System.out.println("\nThe WebGraph is empty.");
			return;
		}
		
		if(sortMethod == 0) {
			// This is the same as sorting without IndexComparator
			Collections.sort(pages, new IndexComparator());
		} else if(sortMethod == 1) {
			Collections.sort(pages, new URLComparator());
		} else if(sortMethod == 2) {
			Collections.sort(pages, new RankComparator());
		}
		
		System.out.println("\nIndex     URL               PageRank  Links               Keywords");
		System.out.println("---------------------------------------------------------------------------------------------------");
		
		for(int i=0; i<pages.size(); i++) {
			String linksString = "";
			
			for(int k=0; k<pages.size(); k++) {
				if(links[pages.get(i).getIndex()][k] == 1) {
					linksString += k + ", ";
				}
			}
			
			if(!linksString.isEmpty()) {
				linksString = linksString.substring(0, linksString.length() - 2);
			}
			
			System.out.println(pages.get(i).toString().replace("***", String.format("%-17s", linksString)));
			
		}
		
		// Reset the order back to sorted by index
		Collections.sort(pages);
		
	}

	/**
	 * Finds and returns the index of the WebPage that matches the input URL
	 * 
	 * @param url
	 * 	The URL to search this WebGraph for
	 * 
	 * @return
	 * 	Returns the index of the WebPage if found, otherwise it returns -1
	 */
	public int indexOfURL(String url) {
		for(int i=0; i<pages.size(); i++) {
			if(pages.get(i).getURL().equals(url)) {
				return i;
			}
		}
		
		return -1;
	}
}
