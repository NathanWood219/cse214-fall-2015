/**
* The <code>RankComparator</code> class allows a WebGraph to be sorted based on rank.
* 
* @author Nathan Wood
*    e-mail: nathan.wood@stonybrook.edu
*    Stony Brook ID: 109876625
 * <dt><b>Assignment:</b><dd>
 *    Homework #7 for CSE 214, Fall 2015
 * <dt><b>Date:</b><dd>
 *    December 1st, 2015
 **/

import java.util.Comparator;

public class RankComparator implements Comparator<Object> {
	
	/**
	 * Compares two WebPage objects to each other based on rank
	 * 
	 * @param o1
	 * 	The first WebPage to compare
	 * 
	 * @param o2
	 * 	The second WebPage to compare
	 * 
	 * @return
	 * 	Returns an integer between -1 and 1 based on the comparison (DSC)
	 */
	public int compare(Object o1, Object o2) {
		WebPage webPage1 = (WebPage)o1;
		WebPage webPage2 = (WebPage)o2;
		
		if(webPage1.getRank() > webPage2.getRank()) {
			return -1;
		} else if(webPage1.getRank() < webPage2.getRank()) {
			return 1;
		}
		return 0;
	}
}
