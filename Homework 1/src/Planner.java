/**
* The <code>Planner</code> class implements an abstract data type for a list of Course objects
*    
* @author Nathan Wood
*    e-mail: nathan.wood@stonybrook.edu
*    Stony Brook ID: 109876625
 * <dt><b>Assignment:</b><dd>
 *    Homework #1 for CSE 214, Fall 2015
 * <dt><b>Date:</b><dd>
 *    September 8th, 2015
 **/

public class Planner {
	// PRIVATE OBJECT VARIABLES
	private Course[] array;
	private final int MAX_COURSES = 50;
	private int size = 0;
	
    /**
    * Constructs an instance of the Planner with no Course objects in it
    *
    * <dt><b>Postconditions</b><dd>
    *   The Planner has been initialized to an empty list of Courses
    **/
	public Planner() {
		array = new Course[MAX_COURSES];
	}
	
    /**
    * Gets the size of the array
    * 
    * <dt><b>Preconditions</b><dd>
    * 	The Planner has been initialized
    *
    * @return
    *   The number of Courses in the Planner
    **/
	public int size() {
		return size;
	}

    /**
    * Inserts a new Course object into the Planner at a specified position
    *
    * @param newCourse
    *   The new Course object to add to the Planner
    *    
    * @param position
    * 	The position at which to insert the new Course object
    * 
    * <dt><b>Preconditions</b><dd>
    * 	The Course object has been initialized, 1<=position<=size+1, and size<MAX_COURSES
    * 
    * <dt><b>Postconditions</b><dd>
    *   The size should increase by one and all Courses after position will be shifted one to the right
    * 
    * @throws IllegalArgumentException
    * 	Indicates that the position argument is out of bounds
    * 
    * @throws FullPlannerException
    * 	Indicates that the Planner has reached the maximum capacity
    **/
	public void addCourse(Course newCourse, int position) throws IllegalArgumentException, FullPlannerException {		
		if(position<1 || position>size+1) {
			throw new IllegalArgumentException();
		}
		if(size==MAX_COURSES) {
			throw new FullPlannerException();
		}
		
		Course temp;
		temp = array[position];
		array[position] = newCourse;
		
		for(int i=position+1; i<=size+1; i++) {
			array[i] = temp;
			temp = array[i+1];
		}
		
		size++;
	}
	
    /**
    * Inserts a new Course object into the Planner at the end of the array
    *
    * @param newCourse
    *   The new Course object to add to the Planner
    * 
    * <dt><b>Preconditions</b><dd>
    * 	The Course object has been initialized and size<MAX_COURSES
    * 
    * <dt><b>Postconditions</b><dd>
    *   The size should increase by one and the new Course will be at the last position
    * 
    * @throws IllegalArgumentException
    * 	Indicates that the position argument is out of bounds (should not happen with this one though)
    * 
    * @throws FullPlannerException
    * 	Indicates that the Planner has reached the maximum capacity
    **/
	public void addCourse(Course newCourse) throws IllegalArgumentException, FullPlannerException {
		addCourse(newCourse,size+1);
	}
	
    /**
    * Removes a Course at position and then collapses the array to remove any gaps
    *
    * @param position
    *   The index of the Course that should be removed
    * 
    * <dt><b>Preconditions</b><dd>
    * 	The Planner has been initialized and 1<=position<=size
    * 
    * <dt><b>Postconditions</b><dd>
    *   The size should decrease by one and the Course previously at position should no longer exist
    * 
    * @throws IllegalArgumentException
    * 	Indicates that the position argument is out of bounds
    **/
	public void removeCourse(int position) throws IllegalArgumentException {
		if(position<1 || position>size+1) {
			throw new IllegalArgumentException();
		}
		
		for(int i=position; i<size; i++) {
			array[i] = array[i+1];
		}
		
		size--;
	}
	
    /**
    * Retrieves the Course at the given position
    *
    * @param position
    *   The index of the Course that is being retrieved
    * 
    * <dt><b>Preconditions</b><dd>
    * 	The Planner has been initialized and 1<=position<=size
    * 
    * @return
    * 	The Course at the specified position in the Planner
    * 
    * @throws IllegalArgumentException
    * 	Indicates that the position argument is out of bounds
    **/
	public Course getCourse(int position) throws IllegalArgumentException {
		if(position<1 || position>size+1) {
			throw new IllegalArgumentException();
		}
		return array[position];
	}
	
    /**
    * Takes in a department code and prints out a list of all the Courses that have the same one
    *
    * @param planner
    *   The list of the courses to search in
    *   
    * @param department
    * 	The 3 letter department code for a Course
    * 
    * <dt><b>Preconditions</b><dd>
    * 	The Planner has been initialized
    * 
    * <dt><b>Postconditions</b><dd>
    * 	A formatted and filtered table will be printed out containing all Courses with that department
    * 	If no courses are found, it informs the user.
    **/
	public static void filter(Planner planner, String department) {
		boolean foundCourses = false;
		for(int i=1; i<=planner.size(); i++) {
			if(planner.getCourse(i).getDepartment().equals(department)) {
				System.out.println("  " + i + " " + planner.getCourse(i).toString());
				foundCourses = true;
			}
		}
		
		if(!foundCourses) {
			System.out.println("  No courses found with the department code '" + department + "'.");
		}
	}
	
    /**
    * Checks whether a certain Course is already in the list
    *
    * @param course
    * 	the Course we are looking for
    * 
    * <dt><b>Preconditions</b><dd>
    * 	The Planner and Course have been initialized
    * 
    * @return
    * 	True if the Planner contains this Course, false otherwise
    **/
	public boolean exists(Course course) {
		for(int i=1; i<=size; i++) {
			if(array[i].equals(course)) {
				return true;
			}
		}
		return false;
	}
	
    /**
    * Finds and returns the first index of a Course in the Planner that matches the input Course
    *
    * @param course
    * 	the Course we are looking for
    * 
    * <dt><b>Preconditions</b><dd>
    * 	The Planner and Course have been initialized
    * 
    * @return
    * 	The index of the Course, -1 if it does not exist in the Planner
    **/
	public int getCourseIndex(Course course) {
		for(int i=1; i<=size; i++) {
			if(array[i].equals(course)) {
				return i;
			}
		}
		
		return -1;
	}
	
    /**
    * Duplicates the Planner and returns a copy with the same attributes
    * 
    * <dt><b>Preconditions</b><dd>
    * 	The Planner has been initialized
    * 
    * @return
    * 	A copy of this Planner object
    **/
	public Object clone() {
		Planner temp = new Planner();
		for(int i=1; i<=size; i++) {
			try {
				temp.addCourse(this.getCourse(i));
			} catch (IllegalArgumentException | FullPlannerException e) {
				// While this should not happen during the clone() because
				// the original planner object had to be valid, calling
				// addCourse() and getCourse() require the exceptions to
				// be handled by clone().
			}
		}
		return temp;
	}
	
    /**
    * Prints a formatted table of each item in the list with its position number
    * 
    * <dt><b>Preconditions</b><dd>
    * 	The Planner has been initialized
    * 
    * <dt><b>Postconditions</b><dd>
    * 	The table will be printed out
    **/
	public void printAllCourses() {
		System.out.println(this.toString());
	}
	
    /**
    * Gets the String representation of the Planner object with position numbers
    * 
    * @return
    * 	The String representation of the Planner
    **/
	public String toString() {
		String output = "";
		Course course;
		for(int i=1; i<=size; i++) {
			course = this.getCourse(i);
			if(i<=9) {
				output += " ";
			}
			output += " " + i + " " + course.toString() + "\n";
		}
		
		return output.substring(0,output.length()-2);
	}
}
