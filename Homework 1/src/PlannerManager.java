/**
* The <code>PlannerManager</code> class is a test program for the Planner and Course classes
*    
* @author Nathan Wood
*    e-mail: nathan.wood@stonybrook.edu
*    Stony Brook ID: 109876625
 * <dt><b>Assignment:</b><dd>
 *    Homework #1 for CSE 214, Fall 2015
 * <dt><b>Date:</b><dd>
 *    September 8th, 2015
 **/

import java.util.InputMismatchException;
import java.util.Scanner;

public class PlannerManager {
	public static void main(String args[] ) {
		// Keyboard input variable
		Scanner input = new Scanner(System.in);
		
		// Empty planners
		Planner testPlanner = new Planner(), backupPlanner = new Planner();
		
		// Required variables to run the menu
		String selection;
		boolean terminate = false;
		
		// Terminate is only true when the user selects (Q) Quit
		while(!terminate) {
			System.out.println("\n(A) Add Course\n(G) Get Course\n(R) Remove Course\n(P) Print Courses in Planner\n(F) Filter by Department Code"
					+ "\n(L) Look for Course\n(S) Size\n(B) Backup\n(PB) Print Courses in Backup\n(RB) Revert to Backup\n(Q) Quit");
			
			// Breaking from this loop returns to the menu
			while(!terminate) {
				// Retrieve the user's menu selection
				System.out.print("\nEnter a selection: ");
				selection = input.nextLine().toUpperCase();
				
				System.out.println(); // Adding spacing
				
				// (A) ADD COURSE
				if(selection.equals("A")) {
					int position;		// position of the course to be added
					Course newCourse;	// temporary variable to hold the properties of the course
					
					// Attempt to create a new course
					try {
						newCourse = createCourse(input);
					} catch(InputMismatchException e) {
						System.out.println("Error: the user input did not match the required data type.");
						input.nextLine(); // Clear user input
						break;
					}
					
					// Get user input for the position
					position = getPosition(input);
					
					// Try to add the course, catch the possible errors while doing so
					try {
						testPlanner.addCourse(newCourse, position);
						// If no exception is thrown above, the following will be printed
						System.out.println("\n" + getCourseDescription(newCourse) + " successfully added to planner.");
					} catch (IllegalArgumentException e) {
						System.out.println("Error: position out of bounds.");
					} catch (FullPlannerException e) {
						System.out.println("Error: planner is full.");
					}
					
					input.nextLine(); // Clear user input
					break;
				}
				
				// (G) GET COURSE
				else if(selection.equals("G")) {
					int position = -1; 	// position of the requested course
					String temp;		// stores the toString() of the course
					
					// Get user input for the position
					position = getPosition(input);
					
					// Attempt to return the properties of the course
					try {
						temp = testPlanner.getCourse(position).toString();
						// If no exception is thrown above, the following will be printed
						printHeader();
						System.out.println("  " + position + " " + temp);
					} catch (IllegalArgumentException e) {
						System.out.println("Error: position out of bounds.");
					}
					
					break;
				}
				
				// (R) REMOVE COURSE
				else if(selection.equals("R")) {
					int position = -1;	// stores the value of the position
					
					// Get user input for the position
					position = getPosition(input);
					
					// Attempt removal of the course
					try {
						// Create a temporary course to store the information for printing afterwards
						Course tempCourse = testPlanner.getCourse(position);
						String description = getCourseDescription(tempCourse);
						
						// Try to remove the course
						testPlanner.removeCourse(position);
						
						// If no exception is thrown above, the following will be printed
						System.out.println("\n" + description + " has been successfully removed from the planner.");
					} catch (IllegalArgumentException e) {
						System.out.println("Error: position out of bounds.");
					} catch (Exception e) {
						System.out.println("Error: the planner has no courses in it.");
					}
					
					break;
				}
				
				// (P) PRINT COURSES IN PLANNER
				else if(selection.equals("P")) {
					// Print the header
					System.out.println("Planner:");
					printHeader();
					
					// Check if the planner has any courses before trying to print the list
					if(testPlanner.size() == 0) {
						System.out.println("  No courses found in the planner.");
					}
					else {
						testPlanner.printAllCourses();
					}
					
					break;
				}
				
				// (F) FILTER BY DEPARTMENT CODE
				else if(selection.equals("F")) {
					// Get the department code from the user
					System.out.print("Enter department code: ");
					String department = input.nextLine().toUpperCase();
					
					System.out.println(); // adding spacing
					
					// Printing out the list of filtered courses
					printHeader();
					Planner.filter(testPlanner, department);
					
					break;
				}
				
				// (L) LOOK FOR COURSE
				else if(selection.equals("L")) {
					Course tempCourse;
					// Attempt to create a new course
					try {
						tempCourse = createCourse(input);
					} catch(InputMismatchException e) {
						System.out.println("Error: the user input did not match the required data type.");
						input.nextLine();
						break;
					}
					
					int position = -1;
					
					System.out.println(); // adding spacing
					
					// Check if the course exists, and if it does then get the position
					if(testPlanner.exists(tempCourse)) {
						position = testPlanner.getCourseIndex(tempCourse);
					}
					
					// If position == -1, then the course does not exist
					if(position==-1) {
						System.out.println("Course not found in the planner.");
					}
					else {
						System.out.println(getCourseDescription(tempCourse) + " is found in the planner at position " + position + ".");
					}
					
					break;
				}
				
				// (S) GET THE SIZE
				else if(selection.equals("S")) {
					System.out.println("There are " + testPlanner.size() + " courses in the planner.");
					
					break;
				}
				
				// (B) BACKUP THE PLANNER
				else if(selection.equals("B")) {
					// Clone the testPlanner and assign it to backupPlanner
					backupPlanner = (Planner)testPlanner.clone();
					System.out.println("Created a backup of the current planner.");
					
					break;
				}
				
				// (PB) PRINT THE COURSES IN BACKUP
				else if(selection.equals("PB")) {
					// Print the headers
					System.out.println("Planner (Backup):");
					printHeader();
					
					// Check if the backup exists yet or not
					if(backupPlanner.size() == 0) {
						System.out.println("  No courses found in the backup planner.");
					}
					else {
						backupPlanner.printAllCourses();
					}
					
					break;
				}
				
				// (RB) REVERT TO THE BACKUP
				else if(selection.equals("RB")) {
					// Clone the backupPlanner and assign it to testPlanner
					testPlanner = (Planner)backupPlanner.clone();
					System.out.println("Planner successfully reverted to the backup copy.");
					
					break;
				}
				
				// (Q) QUIT THE PROGRAM
				else if(selection.equals("Q")) {
					terminate = true;
					
					break;
				}
				
				// If the user did not enter a valid menu option
				else {
					System.out.println("Please enter an option from the menu.");
				}
			}
		}
		
		// This will only occur once the program has been told to terminate
		System.out.println("Program terminating successfully...");
		input.close();
	}
	
    /**
    * Prints out the header information for the courses in a Planner
    *
    * <dt><b>Postconditions</b><dd>
    *   The table should be printed out to the screen
    **/
	public static void printHeader() {
		System.out.println("No. Course Name               Department Code Section Instructor");
		System.out.println("-------------------------------------------------------------------------------");
	}
	
    /**
    * Takes in a Course and outputs a simple String description of it
    * 
    * @param course
    * 	The course to get the information from
    * 
    * @return
    * 	The String description of a course using department, code, and section with formatting
    **/
	public static String getCourseDescription(Course course) {
		return String.format("%3s %3d.%02d", course.getDepartment(), course.getCode(), course.getSection());
	}
	
    /**
    * Retrieves all the information from the user that is required to construct a Course
    * 
    * @param input
    * 	The Scanner object to use for user input
    * 
    * @return
    * 	Returns the newly created Course with user-defined attributes
    * 
    * @throws InputMismatchException
    * 	Indicates that the user input incorrect data types for the fields
    **/
	public static Course createCourse(Scanner input) {		
		String courseName = "", department = "", instructor = "";
		int code = 0;
		byte section = 0;
		
		System.out.print("Enter course name: ");
		courseName = input.nextLine();
		
		System.out.print("Enter department: ");
		department = input.nextLine().toUpperCase();
		
		System.out.print("Enter course code: ");
		code = input.nextInt();
		
		System.out.print("Enter course section: ");
		section = input.nextByte();
		input.nextLine();
		
		System.out.print("Enter instructor: ");
		instructor = input.nextLine();
		
		return new Course(courseName, department, code, section, instructor);
	}
	
    /**
    * Retrieves a position index from the user
    * 
    * @param input
    * 	The Scanner object to use for user input
    * 
    * @return
    * 	Returns the integer to be used for the position in the Planner array
    * 
    * @throws InputMismatchException
    * 	Indicates that the user did not enter an integer
    **/
	public static int getPosition(Scanner input) {
		int position = -1;
		try {
			System.out.print("Enter position: ");
			position = input.nextInt();
		} catch (InputMismatchException e) {
			System.out.println("Error: the user input did not match the required data type.");
		}
		return position;
	}
}
