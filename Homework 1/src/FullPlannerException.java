/**
* A <code>FullPlannerException</code> will be thrown if a Planner object
* has reached its max capacity.
* 
* @author Nathan Wood
*    e-mail: nathan.wood@stonybrook.edu
*    Stony Brook ID: 109876625
 * <dt><b>Assignment:</b><dd>
 *    Homework #1 for CSE 214, Fall 2015
 * <dt><b>Date:</b><dd>
 *    September 8th, 2015
 **/
public class FullPlannerException extends Exception {

	private static final long serialVersionUID = -851058554559594055L;

    /**
     * Default constructor for an <CODE>FullPlannerException</CODE> that
     * passes a default string to the <CODE>Exception</CODE> class constructor.
     *
     * <dt><b>Postcondition:</b><dd>
     *    The object is created and contains the default message.
     */
	public FullPlannerException() {
		// Default message
		super("Error: planner is full.");
	}
	
    /**
     * Second constructor for the <CODE>FullPlannerException</CODE> that
     * passes a provided string to the <CODE>Exception</CODE> class constructor.
     *
     * @param message
     *    the message that the object is to contain
     * <dt><b>Postcondition:</b><dd>
     *    The object is created and contains the provided message.
     */
    public FullPlannerException(String message) {   
    	// Custom message
        super(message);
    }
}
