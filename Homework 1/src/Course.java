/**
* The <code>Course</code> class contains general information about any given Course
*    
* @author Nathan Wood
*    e-mail: nathan.wood@stonybrook.edu
*    Stony Brook ID: 109876625
* <dt><b>Assignment:</b><dd>
*    Homework #1 for CSE 214, Fall 2015
* <dt><b>Date:</b><dd>
*    September 8th, 2015
**/

public class Course {
	// PRIVATE OBJECT VARIABLES
	private String courseName = "", department = "", instructor = "";
	private int code;
	private byte section;
	
	// CONSTRUCTORS
	
    /**
    * Constructs an instance of the Course object using the given parameters
    * 
    * @param courseName
    * 	The name of the Course as a String
    * 
    * @param department
    * 	The 3 letter department code as a String
    * 
    * @param code
    * 	The 3 number code for the class as an int
    * 
    * @param section
    * 	The section number as byte
    * 
    * @param instructor
    * 	The name of the instructor as a String
    *
    * <dt><b>Postconditions</b><dd>
    *   The Course has been initialized with the specified arguments
    **/
	public Course(String courseName, String department, int code, byte section, String instructor) {
		this.courseName = courseName;
		this.department = department;
		this.code = code;
		this.section = section;
		this.instructor = instructor;
	}
	
	// MUTATORS
	
    /**
    * Sets courseName to the given String
    *
    * @param courseName
    *    The new name of the course
    **/
	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

    /**
    * Sets department to the given String
    *
    * @param department
    *    The new name of the department
    **/
	public void setDepartment(String department) {
		this.department = department;
	}

    /**
    * Sets code to the given int
    *
    * @param code
    *    The new code number
    **/
	public void setCode(int code) {
		this.code = code;
	}

    /**
    * Sets section to the given byte
    *
    * @param section
    *    The new section number
    **/
	public void setSection(byte section) {
		this.section = section;
	}

    /**
    * Sets instructor to the given String
    *
    * @param instructor
    *    The new name of the instructor
    **/
	public void setInstructor(String instructor) {
		this.instructor = instructor;
	}
	
	// ACCESSORS
	
	/**
	* Returns the name of the course
	*
	* @return
	*   Returns the name of the course as a String
	**/
	public String getCourseName() {
		return courseName;
	}
	
	/**
	* Returns the name of the department
	*
	* @return
	*   Returns the name of the department as a String
	**/
	public String getDepartment() {
		return department;
	}

	/**
	* Returns the code number
	*
	* @return
	*   Returns the code as an int
	**/
	public int getCode() {
		return code;
	}

	/**
	* Returns the section number
	*
	* @return
	*   Returns the section as a byte
	**/
	public byte getSection() {
		return section;
	}
	
	/**
	* Returns the name of the instructor
	*
	* @return
	*   Returns the name of the instructor as a String
	**/
	public String getInstructor() {
		return instructor;
	}
	
	/**
	* Returns a string that contains all the course information
	*
	* @return
	*   Returns the course information as a String
	**/
	public String toString() {
		return String.format("%-26s%-7s%8d      %02d %-20s", courseName, department, code, section, instructor);
	}
	
	// METHODS
	
    /**
    * Returns a cloned copy of the Course object
    *
    * @return 
    *   Returns a cloned copy of the Course that contains all the same attributes
    **/
	public Object clone() {
		Course obj = new Course(courseName, department, code, section, instructor);
		return obj;
	}
	
    /**
    * Compares the attributes of two Course objects and returns true or false if they are equal
    *
    * @param obj
    * 	The name of the Course object that will be compared to this one
    *
    * @return 
    *   Returns false if obj is not a Course object, or returns true/false based on the attributes
    **/
	public boolean equals(Object obj) {
		if(obj instanceof Course) {
			Course test = (Course)obj;
			
			return (test.courseName.equals(courseName)) && (test.department.equals(department)) && 
					(test.code == code) && (test.section == section) && (test.instructor.equals(instructor));
		}
		
		return false;
	}
}
