/**
* A <code>RoutersFullException</code> will be thrown if all the routers
* in a collection have reached their max capacity.
* 
* @author Nathan Wood
*    e-mail: nathan.wood@stonybrook.edu
*    Stony Brook ID: 109876625
 * <dt><b>Assignment:</b><dd>
 *    Homework #4 for CSE 214, Fall 2015
 * <dt><b>Date:</b><dd>
 *    October 20th, 2015
 **/
public class RoutersFullException extends Exception {

	private static final long serialVersionUID = -2561529883839645777L;

	/**
     * Default constructor for an <CODE>FullQueueException</CODE> that
     * passes a default string to the <CODE>Exception</CODE> class constructor.
     *
     * <dt><b>Postcondition:</b><dd>
     *    The object is created and contains the default message.
     */
	public RoutersFullException() {
		// Default message
		super("All routers are full.");
	}
	
    /**
     * Second constructor for the <CODE>FullQueueException</CODE> that
     * passes a provided string to the <CODE>Exception</CODE> class constructor.
     *
     * @param message
     *    the message that the object is to contain
     * <dt><b>Postcondition:</b><dd>
     *    The object is created and contains the provided message.
     */
    public RoutersFullException(String message) {   
    	// Custom message
        super(message);
    }
}
