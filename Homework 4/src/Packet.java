/**
* The <code>Packet</code> class stores the information about an individual packet.
*    
* @author Nathan Wood
*    e-mail: nathan.wood@stonybrook.edu
*    Stony Brook ID: 109876625
* <dt><b>Assignment:</b><dd>
*    Homework #4 for CSE 214, Fall 2015
* <dt><b>Date:</b><dd>
*    October 20th, 2015
**/

public class Packet {
	// ATTRIBUTES
	static int packetCount = 0;
	int id, packetSize, timeArrive, timeToDest;
	
	// CONSTRUCTORS
	
	/**
	 * Default constructor for a Packet object
	 * 
	 * @param packetSize
	 * 	The size of the packet
	 * 
	 * @param timeArrive
	 * 	The time at which the Packet was created
	 * 
	 * <dt><b>Postcondition:</b><dd>
	 * 	This Packet object will have been initialized and packetCount will have been incremented
	 */
	public Packet(int packetSize, int timeArrive) {
		this.id = ++packetCount;
		this.packetSize = packetSize;
		this.timeArrive = timeArrive;
		this.timeToDest = packetSize / 100;
	}
	
	// ACCESSORS
	
	/**
	 * Returns the packetCount for all the Packet objects
	 * 
	 * @return
	 * 	The packetCount (static)
	 */
	public static int getPacketCount() {
		return packetCount;
	}
	
	/**
	 * Returns the ID of this Packet object
	 * 
	 * @return
	 * 	The ID of this Packet
	 */
	public int getID() {
		return id;
	}
	
	/**
	 * Returns the size of this Packet object
	 * 
	 * @return
	 * 	The size of this Packet
	 */
	public int getPacketSize() {
		return packetSize;
	}

	/**
	 * Returns the arrival time of this Packet object
	 * 
	 * @return
	 * 	The arrival time of this Packet
	 */
	public int getTimeArrive() {
		return timeArrive;
	}

	/**
	 * Returns the time left until reaching the destination for this Packet object
	 * 
	 * @return
	 * 	The time left for this Packet
	 */
	public int getTimeToDest() {
		return timeToDest;
	}
	
	// MUTATORS

	/**
	 * Sets the packetCount for all the Packet objects (static)
	 * 
	 * @param pCount
	 * 	The updated number for packetCount
	 */
	public static void setPacketCount(int pCount) {
		packetCount = pCount;
	}

	/**
	 * Sets the ID of this Packet
	 * 
	 * @param id
	 * 	The new ID for this Packet
	 */
	public void setID(int id) {
		this.id = id;
	}

	/**
	 * Sets the ID of this Packet
	 * 
	 * @param id
	 * 	The new ID for this Packet
	 */
	public void setPacketSize(int packetSize) {
		this.packetSize = packetSize;
	}

	/**
	 * Sets the arrival time of this Packet
	 * 
	 * @param timeArrive
	 * 	The new timeArrive for this Packet
	 */
	public void setTimeArrive(int timeArrive) {
		this.timeArrive = timeArrive;
	}

	/**
	 * Sets the destination time of this Packet
	 * 
	 * @param timeToDest
	 * 	The new timeToDest for this Packet
	 */
	public void setTimeToDest(int timeToDest) {
		this.timeToDest = timeToDest;
	}
	
	// METHODS

	/**
	 * Decrements the time left till reaching the destination for this Packet
	 * 
	 * <dt><b>Postcondition:</b><dd>
	 * 	The timeToDest for this Packet will be decremented
	 */
	public void decrementTime() {
		this.timeToDest--;
	}
	
	/**
	 * Returns a String representation of this Packet object containing the ID,
	 * 	arrival time, and time left to destination
	 * 
	 * @return
	 * 	A formatted String containing the attributes of this Packet
	 */
	public String toString() {
		return String.format("[%d, %d, %d]", id, timeArrive, timeToDest);
	}
}
