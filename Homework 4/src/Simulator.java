/**
* The <code>Simulator</code> class allows a user to run test simulations using Packet and Router.
*    
* @author Nathan Wood
*    e-mail: nathan.wood@stonybrook.edu
*    Stony Brook ID: 109876625
* <dt><b>Assignment:</b><dd>
*    Homework #4 for CSE 214, Fall 2015
* <dt><b>Date:</b><dd>
*    October 20th, 2015
**/

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Simulator {
	// Static variables
	public static final int MAX_PACKETS = 3;				// Number of packets that can arrive per simulation unit
	
	static int totalServiceTime = 0;						// Contains running sum of total time
	static int totalPacketsArrived = 0;						// Contains total number of packets that were accepted at destination
	static int packetsDropped = 0;							// Records number of dropped packets due to congestion
	
	public static void main(String args[] ) {
		Scanner input = new Scanner(System.in);
		
		double arrivalProb;									// Probability of a new packet arriving at the Dispatcher
		int numIntRouters;									// Number of intermediate routers in the network
		int maxBufferSize;									// Maximum number of Packets a Router can fit
		int minPacketSize;									// Minimum size of a Packet
		int maxPacketSize;									// Maximum size of a Packet
		int bandwidth;										// Maximum number of Packets the destination Router can accept per unit
		int duration;										// Number of simulation units
		
		// Continue with new simulations until the user quits
		while(true) {
			// Reset the value of all the static variables prior to starting another simulation
			totalServiceTime = 0;
			totalPacketsArrived = 0;
			packetsDropped = 0;
			Packet.setPacketCount(0);
			
			// Begin the simulation and retrieve user input for each option
			System.out.println("Starting simulator...");
			
			// All of these use a function that loops with a try and catch to handle InputMismatchExceptions and verifies the range
			numIntRouters = getUserInt(input, "\nEnter the number of Intermediate routers: ", 0, -1);
			arrivalProb = getUserDouble(input,"\nEnter the arrival probability of a packet: ", 0, 1);
			maxBufferSize = getUserInt(input, "\nEnter the maximum buffer size of a router: ", 0, -1);
			minPacketSize = getUserInt(input, "\nEnter the minimum size of a packet: ", 0, -1);
			maxPacketSize = getUserInt(input, "\nEnter the maximum size of a packet: ", minPacketSize, -1);
			bandwidth = getUserInt(input, "\nEnter the bandwidth size: ", 0, -1);
			duration = getUserInt(input, "\nEnter the simulation duration: ", 0, -1);
			
			// Adding spacing
			System.out.println();
			
			// Run the simulation and retrieve the averageTime
			double averageTime = simulate(numIntRouters, arrivalProb, maxBufferSize, minPacketSize, maxPacketSize, bandwidth, duration);
			
			// Final simulation output
			System.out.println("Simulation ending...");
			System.out.println("Total service time: " + totalServiceTime);
			System.out.println("Total packets served: " + totalPacketsArrived);
			System.out.println("Average service time per packet: " + roundDecimal(averageTime, 4));
			System.out.println("Total packets dropped: " + packetsDropped);
			
			// Clear user input
			input.nextLine();
			
			// Request user input
			System.out.print("\nDo you want to try another simulation? (y/n): ");
			String userInput = input.nextLine().toLowerCase();
			
			// Break the while loop if the user wishes to quit
			if(userInput.equals("n")) {
				break;
			}
			
			// Adding spacing
			System.out.println();
		}
		
		// This will only be reached if the while loop is broken by the user
		System.out.println("Program terminating successfully...");
		input.close();
	}
	
	public static double simulate(int numIntRouters, double arrivalProb, int maxBufferSize, int minPacketSize, int maxPacketSize, int bandwidth, int duration) {
		
		Router dispatcher = new Router(MAX_PACKETS);				// Level 1 router
		ArrayList<Router> routers = new ArrayList<Router>();		// Level 2 routers
		Router destination = new Router();							// Final destination router
		
		// Creating the Level 2 Intermediate routers
		for(int i=0; i<numIntRouters; i++) {
			routers.add(new Router(maxBufferSize));
		}
		
		// Goes through every simulation unit
		for(int simUnit = 1; simUnit<=duration; simUnit++) {
			
			// Print the time
			System.out.println("Time: " + simUnit);
			
			int packetsArrived = 0;
			
			// 1. Tries to generate the max number of packets
			for(int i=0; i<MAX_PACKETS; i++) {
				
				// Decide whether packets have arrived at the Dispatcher
				if(Math.random() < arrivalProb) {
					// Generate a random size within the range and create a new packet
					Packet newPacket = new Packet(randInt(minPacketSize, maxPacketSize), simUnit);
					
					// Add the packet to the dispatcher and increment the count
					dispatcher.enqueue(newPacket);
					
					// Print the update to the console
					System.out.println("Packet " + newPacket.getID() + " arrives at dispatcher with size " + newPacket.getPacketSize() + ".");
					
					packetsArrived++;
					
				}
			}
			
			// No packets arrived
			if(packetsArrived==0) {
				System.out.println("No packets arrived.");
			}
				
			// 2. Send packets from Dispatcher to Intermediate Routers [ sendPacketTo(Collection intRouters) ]
			while(!dispatcher.isEmpty()) {
				try {
					Packet packet = dispatcher.dequeue();
					
					try {
						// First make sure that there is at least one intermediary Router
						if(numIntRouters<=0) {
							throw new RoutersFullException();
						}
						
						// Find the router with the least number of packets
						int index = Router.sendPacketTo(routers);
						
						// If all the Routers are full, an exception will have been thrown and the code will not reach this point					
						routers.get(index).enqueue(packet);
						System.out.println("Packet " + packet.getID() + " sent to Router " + (index+1) + ".");
					} catch(RoutersFullException e) {
						// Routers are all full
						System.out.println("Network is congested. Packet " + packet.getID() + " is dropped.");
						packetsDropped++;
					}
				} catch(EmptyQueueException e) {
					e.printStackTrace();
				}
			}
			
			// 3. Decrement all packets counter
			// 4. Forward packets to Destination router if ready (take note of bandwidth)
			// 5. Record arrive time upon Destination and print out to the user
			int numSent = 0;
			String routersOutput = "";
			
			try {
				for(int i=0; i<numIntRouters; i++) {
					if(!routers.get(i).isEmpty()) {
						Packet packet = routers.get(i).peek();
						packet.decrementTime();
						
						if(packet.getTimeToDest()<=0 && numSent<bandwidth) {
							destination.enqueue(routers.get(i).dequeue());
							int timeSpent = simUnit - packet.getTimeArrive();
							System.out.println("Packet " + packet.getID() + " has successfully reached its destination: +" + timeSpent);
							totalServiceTime += timeSpent;
							totalPacketsArrived++;
							numSent++;
						}
					}
					
					// Add to the Routers collection output String
					routersOutput += "R" + (i+1) + ": " + routers.get(i).toString() + "\n";
				}
			} catch(EmptyQueueException e) {
				e.printStackTrace();
			}

			// 6. Print the intermediate routers
			System.out.println(routersOutput);
		}
		
		// Return average service time
		return totalServiceTime*1.0/totalPacketsArrived;
	}

	/**
	 * Returns a random int between minVal and maxVal
	 * 
	 * @param minVal
	 * 	The minimum value for the range of random numbers
	 * 
	 * @param maxVal
	 * 	The maximum value for the range of random numbers
	 * 
	 * @return
	 * 	Random value between minVal and maxVal
	 */
	private static int randInt(int minVal, int maxVal) {
		return (int)((Math.random() * (maxVal - minVal)) + minVal);
	}
	
	/**
	 * Requests for an int from the user with a specified message until it is the right data type and range
	 * 
	 * @param input
	 * 	The Scanner to use to request user input
	 * 
	 * @param message
	 * 	The prompt that will be printed out to the user
	 * 
	 * @param min
	 * 	The minimum accepted value, -1 if there is no min
	 * 
	 * @param max
	 * 	The maximum accepted value, -1 if there is no max
	 * 
	 * @return
	 * 	The user specified integer
	 */
	public static int getUserInt(Scanner input, String message, int min, int max) {
		int output;
		
		while(true) {
			try {
				System.out.print(message);
				output = input.nextInt();
				
				if((min != -1 && output<min) || (max != -1 && output>max)) {
					if(max == -1) {
						System.out.println("Error: input must be greater than " + min + ".");
					} else if(min == -1) {
						System.out.println("Error: input must be less than " + max + ".");
					} else {
						System.out.println("Error: input is out of the accepted range (" + min + " - " + max + ").");
					}
					input.nextLine();
					continue;
				}
				
				break;
				
			} catch(InputMismatchException e) {
				System.out.println("Error: input did not match required type.");
				input.nextLine();
			}
		}
		
		return output;
	}
	
	/**
	 * Requests for a double from the user with a specified message until it is the right data type and range
	 * 
	 * @param input
	 * 	The Scanner to use to request user input
	 * 
	 * @param message
	 * 	The prompt that will be printed out to the user
	 * 
	 * @param min
	 * 	The minimum accepted value, -1 if there is no min
	 * 
	 * @param max
	 * 	The maximum accepted value, -1 if there is no max
	 * 
	 * @return
	 * 	The user specified double
	 */
	public static double getUserDouble(Scanner input, String message, double min, double max) {
		double output;
		
		while(true) {
			try {
				System.out.print(message);
				output = input.nextDouble();
				
				if((min != -1 && output<min) || (max != -1 && output>max)) {
					if(max == -1) {
						System.out.println("Error: input must be greater than " + min + ".");
					} else if(min == -1) {
						System.out.println("Error: input must be less than " + max + ".");
					} else {
						System.out.println("Error: input is out of the accepted range (" + min + " - " + max + ").");
					}
					input.nextLine();
					continue;
				}
				
				break;
				
			} catch(InputMismatchException e) {
				System.out.println("Error: input did not match required type.");
				input.nextLine();
			}
		}
		
		return output;
	}
	
	/**
	 * Takes in a double and a desired number of decimal places and returns a formatted double
	 * 
	 * @param decimal
	 * 	The double that will be formatted
	 * 
	 * @param decimalPlace
	 * 	The number of decimal places to round to
	 * 
	 * @return
	 * 	The rounded double, typically for cleaner print formatting
	 */
	private static double roundDecimal(double decimal, int decimalPlace) {
		return (double)Math.round(decimal * Math.pow(10, decimalPlace)) / Math.pow(10, decimalPlace);
	}
}
