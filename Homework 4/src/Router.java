/**
* The <code>Router</code> class is a queue implemented using circular arrays and contains Packets.
*    
* @author Nathan Wood
*    e-mail: nathan.wood@stonybrook.edu
*    Stony Brook ID: 109876625
* <dt><b>Assignment:</b><dd>
*    Homework #4 for CSE 214, Fall 2015
* <dt><b>Date:</b><dd>
*    October 20th, 2015
**/

import java.util.ArrayList;

public class Router {
	// ATTRIBUTES
	private int size = 0, capacity = 3, front = 0, rear = -1;
	private Packet[] queue;
	
	// CONSTRUCTORS
	
	/**
	 * Default constructor for a Router object
	 * 
	 * <dt><b>Postcondition:</b><dd>
	 * 	This Router object will have been initialized with the default values
	 */
	public Router() {
		this.queue = new Packet[capacity];
	}
	
	/**
	 * Secondary constructor for a Router object
	 * 
	 * @param capacity
	 * 	Maximum number of Packets that this Router queue can hold initially
	 * 		Because the queues automatically expand in size, this only changes the INITIAL capacity,
	 * 		meaning that it will prevent new instances from being added in sendPacketTo() beyond INITIAL capacity
	 * 
	 * <dt><b>Postcondition:</b><dd>
	 * 	This Router object will have been initialized with an initial capacity
	 */
	public Router(int capacity) {
		this.capacity = capacity;
		this.queue = new Packet[capacity];
	}
	// METHODS
	
	/**
	 * Inserts a new Packet at the rear of the Router queue in O(1)
	 * 
	 * @param packet
	 * 	The Packet to add to the queue
	 * 
	 * <dt><b>Postcondition:</b><dd>
	 * 	The Packet will be at the rear and size will be incremented
	 */
	public void enqueue(Packet packet) {
		if(rear+1==capacity) {
			rear = 0;
		} else {
			rear++;
		}
		
		if(this.isFull()) {
			this.expandCapacity();
		}
		
		size++;
		queue[rear] = packet;
	}

	/**
	 * Removes and returns the Packet at the front of the Router queue in O(1)
	 * 
	 * @throws EmptyQueueException
	 * 	Indicates that no Packets existed in this Router
	 * 
	 * @return
	 * 	Returns the Packet at the front of the queue
	 * 
	 * <dt><b>Postcondition:</b><dd>
	 * 	The front of the queue will be shifted forwards and size will be decremented
	 */
	public Packet dequeue() throws EmptyQueueException {
		if(this.isEmpty()) {
			throw new EmptyQueueException();
		}
		
		Packet packet = queue[front];
		
		if(front==capacity-1) {
			front = 0;
		} else {
			front++;
		}
		
		size--;
		return packet;
	}

	/**
	 * Returns the Packet at the front of the Router queue in O(1)
	 * 
	 * @throws EmptyQueueException
	 * 	Indicates that no Packets existed in this Router
	 * 
	 * @return
	 * 	Returns the Packet at the front of the queue
	 */
	public Packet peek() throws EmptyQueueException {
		if(this.isEmpty()) {
			throw new EmptyQueueException();
		}
		
		return queue[front];
	}

	/**
	 * Returns the size of this Router in O(1)
	 * 
	 * @return
	 * 	Returns the size of the queue
	 */
	public int size() {
		return size;
	}

	/**
	 * Returns true/false depending on whether the Router queue is empty or not in O(1)
	 * 
	 * @return
	 * 	Returns true if the queue is empty
	 */
	public boolean isEmpty() {
		return (size == 0);
	}
	
	/**
	 * Returns true/false depending on whether the Router queue is empty or not in O(1)
	 * 
	 * @return
	 * 	Returns true if the queue is empty
	 */
	public boolean isFull() {
		return (size == capacity);
	}
	
	/**
	 * Expands the size of the queue array to contain more Packets in O(n)
	 * 
	 * <dt><b>Postcondition:</b><dd>
	 * 	Capacity will be doubled and the new front will be at position 0
	 */
	private void expandCapacity() {
		Packet[] expandedQueue = new Packet[(capacity * 2) + 1];
		
		int range = (rear<front ? capacity-1 : rear);
		
		int expandedQueueIndex = 0;
		
		for(int i=front; i<=range; i++) {
			expandedQueue[expandedQueueIndex++] = queue[i];
		}
		
		if(range==capacity-1) {
			for(int i=0; i<=rear; i++) {
				expandedQueue[expandedQueueIndex++] = queue[i];
			}
		}
		
		this.front = 0;
		this.rear = size - 1;
		this.capacity = (capacity * 2) + 1;
		this.queue = expandedQueue;
	}

	/**
	 * Returns a String representation of this Router queue in O(n)
	 * 
	 * @return
	 * 	A formatted String containing the attributes of each Packet
	 */
	public String toString() {
		String output = "{";
		Packet packet;
		
		if(size==0) {
			return "{}";
		}
		
		try {
			for(int i=0; i<size; i++) {
				packet = this.dequeue();
				output += packet.toString() + ", ";
				this.enqueue(packet);
			}
		} catch(EmptyQueueException e) {
			e.printStackTrace();
		}
		
		return output.substring(0,output.length()-2) + "}";
	}

	/**
	 * Returns the index of the Router with the least number of Packets in O(n)
	 * 		Instead of needing to throw an exception, it returns -1 instead.
	 * 
	 * @throws RoutersFullException
	 * 	Indicates that none of the Routers have room for more Packets, therefore the stream is congested
	 * 
	 * @return
	 * 	Returns either the index of the least-filled Router or -1 if all of them are full
	 */
	public static int sendPacketTo(ArrayList<Router> routers) throws RoutersFullException {
		int minLength = routers.get(0).size()+1;
		int index = -1;
		int currentLength = 0;
		
		for(int i=0; i<routers.size(); i++) {
			currentLength = routers.get(i).size();
			if(currentLength<minLength) {
				minLength = currentLength;
				index = i;
			}
		}
		
		if(routers.get(index).isFull()) {
			throw new RoutersFullException();
		}
		
		return index;
	}
}
