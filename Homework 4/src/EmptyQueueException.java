/**
* A <code>EmptyQueueException</code> will be thrown if a Queue object
* is empty and peek() or dequeue() has been called.
* 
* @author Nathan Wood
*    e-mail: nathan.wood@stonybrook.edu
*    Stony Brook ID: 109876625
 * <dt><b>Assignment:</b><dd>
 *    Homework #4 for CSE 214, Fall 2015
 * <dt><b>Date:</b><dd>
 *    October 20th, 2015
 **/
public class EmptyQueueException extends Exception {

	private static final long serialVersionUID = -4556345733457068660L;

	/**
     * Default constructor for an <CODE>EmptyQueueException</CODE> that
     * passes a default string to the <CODE>Exception</CODE> class constructor.
     *
     * <dt><b>Postcondition:</b><dd>
     *    The object is created and contains the default message.
     */
	public EmptyQueueException() {
		// Default message
		super("Queue is empty.");
	}
	
    /**
     * Second constructor for the <CODE>EmptyQueueException</CODE> that
     * passes a provided string to the <CODE>Exception</CODE> class constructor.
     *
     * @param message
     *    the message that the object is to contain
     * <dt><b>Postcondition:</b><dd>
     *    The object is created and contains the provided message.
     */
    public EmptyQueueException(String message) {   
    	// Custom message
        super(message);
    }
}
