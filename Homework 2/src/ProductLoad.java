/**
* The <code>ProductLoad</code> class is used by TrainCar to determine specific details for each car
*    
* @author Nathan Wood
*    e-mail: nathan.wood@stonybrook.edu
*    Stony Brook ID: 109876625
* <dt><b>Assignment:</b><dd>
*    Homework #2 for CSE 214, Fall 2015
* <dt><b>Date:</b><dd>
*    September 21st, 2015
**/

public class ProductLoad {
	// ATTRIBUTES
	
	private String name;
	private double weight, value;
	private boolean isDangerous;
	
	// CONSTRUCTORS
	
	/**
	 * Default constructor for a ProductLoad object
	 * 
	 * @param name
	 * 	The desired name of the ProductLoad
	 * 
	 * @param weight
	 * 	The weight of the load in tons
	 * 
	 * @param value
	 * 	The value of the load in dollars
	 * 
	 * @param isDangerous
	 * 	Whether the load is dangerous or not
	 * 
	 * <dt><b>Postcondition:</b><dd>
	 * 	This ProductLoad will have been initialized
	 */
	public ProductLoad(String name, double weight, double value, boolean isDangerous) {
		this.name = name;
		this.weight = weight;
		this.value = value;
		this.isDangerous = isDangerous;
	}
	
	// ACCESSORS
	
	/**
	 * Returns the name of this ProductLoad
	 * 
	 * @return
	 * 	The name of this ProductLoad
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Returns the weight of this ProductLoad
	 * 
	 * @return
	 * 	The weight of this ProductLoad
	 */
	public double getWeight() {
		return weight;
	}
	
	/**
	 * Returns the value of this ProductLoad
	 * 
	 * @return
	 * 	The value of this ProductLoad
	 */
	public double getValue() {
		return value;
	}
	
	/**
	 * Returns whether this ProductLoad is dangerous or not
	 * 
	 * @return
	 * 	Returns true if the load is dangerous
	 */
	public boolean isDangerous() {
		return isDangerous;
	}
	
	// MUTATORS
	
	/**
	 * Sets the name of the ProductLoad
	 * 
	 * @param name
	 * 	The new name for the ProductLoad
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * Sets the weight of the ProductLoad
	 * 
	 * @param weight
	 * 	The new weight for the ProductLoad
	 */
	public void setWeight(double weight) {
		this.weight = weight;
	}
	
	/**
	 * Sets the value of the ProductLoad
	 * 
	 * @param value
	 * 	The new value for the ProductLoad
	 */
	public void setValue(double value) {
		this.value = value;
	}
	
	/**
	 * Sets whether the ProductLoad is dangerous or not
	 * 
	 * @param isDangerous
	 * 	True/False for whether the ProductLoad is dangerous or not
	 */
	public void isDangerous(boolean isDangerous) {
		this.isDangerous = isDangerous;
	}
	
	
}
