/**
* The <code>TrainCarNode</code> class is used by the TrainLinkedList to navigate through TrainCar objects
*    
* @author Nathan Wood
*    e-mail: nathan.wood@stonybrook.edu
*    Stony Brook ID: 109876625
* <dt><b>Assignment:</b><dd>
*    Homework #2 for CSE 214, Fall 2015
* <dt><b>Date:</b><dd>
*    September 21st, 2015
**/

public class TrainCarNode {
	// ATTRIBUTES
	
	private TrainCarNode prev = null, next = null;
	private TrainCar car;
	
	// CONSTRUCTORS
	
	/**
	 * Default constructor for a TrainCarNode object
	 * 
	 * <dt><b>Postcondition:</b><dd>
	 * 	This TrainCarNode will have been initialized
	 */
	public TrainCarNode() {
	}
	
	/**
	 * Secondary constructor for a TrainCarNode object
	 * 
	 * @param car
	 * 	The car to insert in the node
	 * 
	 * <dt><b>Precondition:</b><dd>
	 * 	Car must be initialized
	 * 
	 * <dt><b>Postcondition:</b><dd>
	 * 	This TrainCarNode will have been initialized
	 */
	public TrainCarNode(TrainCar car) {
		this.car = car;
	}
	
	// ACCESSORS
	
	/**
	 * Returns the previous TrainCarNode from this object
	 * 
	 * @return
	 * 	The previous node in the linked list
	 */
	public TrainCarNode getPrev() {
		return prev;
	}
	
	/**
	 * Returns the next TrainCarNode from this object
	 * 
	 * @return
	 * 	The next node in the linked list
	 */
	public TrainCarNode getNext() {
		return next;
	}
	
	/**
	 * Returns the car contained by this TrainCarNode
	 * 
	 * @return
	 * 	The car attached to this node
	 */
	public TrainCar getCar() {
		return car;
	}
	
	// MUTATORS
	
	/**
	 * Sets this node's reference to the previous node
	 * 
	 * @param prev
	 * 	The TrainCarNode to set as the previous node
	 */
	public void setPrev(TrainCarNode prev) {
		this.prev = prev;
	}
	
	/**
	 * Sets this node's reference to the next node
	 * 
	 * @param next
	 * 	The TrainCarNode to set as the next node
	 */
	public void setNext(TrainCarNode next) {
		this.next = next;
	}
	
	/**
	 * Sets this node's TrainCar
	 * 
	 * @param car
	 * 	The TrainCar to set this node's car to
	 */
	public void setCar(TrainCar car) {
		this.car = car;
	}
	
	// METHODS
	
	/**
	 * Returns a String representing the attributes contained by this TrainCarNode
	 * 
	 * @return
	 * 	The String object containing formatted information about this node
	 */
	public String toString() {
		ProductLoad temp = car.getProductLoad();
		
		if(temp==null) {
			return String.format("%,14.1f%,14.1f  |%10s%,14.1f%,14.2f%12s",
					car.getCarLength(),car.getCarWeight(),"Empty",0.0,0.0,"NO");
		}
		
		return String.format("%,14.1f%,14.1f  |%10s%,14.1f%,14.2f%12s",
				car.getCarLength(),car.getCarWeight(),temp.getName(),temp.getWeight(),temp.getValue(),(temp.isDangerous() ? "YES" : "NO"));
	}
}
