/**
* The <code>TrainLinkedList</code> class is a custom linked list using TrainCarNodes
*    
* @author Nathan Wood
*    e-mail: nathan.wood@stonybrook.edu
*    Stony Brook ID: 109876625
* <dt><b>Assignment:</b><dd>
*    Homework #2 for CSE 214, Fall 2015
* <dt><b>Date:</b><dd>
*    September 21st, 2015
**/

public class TrainLinkedList {
	// ATTRIBUTES
	
	private TrainCarNode head = null, cursor = null, tail = null;
	private int size = 0;
	private double length = 0, value = 0, weight = 0;
	private int numDangerous = 0;
	
	// CONSTRUCTORS
	
	/**
	 * Default constructor for a TrainLinkedList
	 * 
	 * <dt><b>Postcondition:</b><dd>
	 * 	This TrainLinkedList will have been initialized
	 */
	public TrainLinkedList() {
	}
	
	// ACCESSORS
	
	/**
	 * Returns the TrainCar that is currently selected by the cursor
	 * 
	 * @return
	 * 	The TrainCar attributed to the TrainCarNode that is under the cursor
	 */
	public TrainCar getCursorData() {
		return (cursor==null ? null : cursor.getCar());
	}
	
	/**
	 * Returns the size of this TrainLinkedList in O(1)
	 * 
	 * @return
	 * 	The number of cars in this TrainLinkedList
	 */
	public int size() {
		return size;
	}
	
	/**
	 * Returns the length of this TrainLinkedList in O(1)
	 * 
	 * @return
	 * 	The length of this TrainLinkedList in meters
	 */
	public double getLength() {
		return length;
	}
	
	/**
	 * Returns the value of this TrainLinkedList in O(1)
	 * 
	 * @return
	 * 	The value of this TrainLinkedList in dollars
	 */
	public double getValue() {
		return value;
	}
	
	/**
	 * Returns the weight of this TrainLinkedList in O(1)
	 * 
	 * @return
	 * 	The weight of this TrainLinkedList in tons
	 */
	public double getWeight() {
		return weight;
	}
	
	/**
	 * Returns whether this TrainLinkedList is dangerous or not in O(1)
	 * 
	 * @return
	 * 	Returns True if there is at least 1 TrainCar that is dangerous
	 */
	public boolean isDangerous() {
		return (numDangerous>0);
	}
	
	// METHODS
	
	/**
	 * Sets the TrainCarNode at cursor to the TrainCar parameter
	 * 
	 * @param car
	 * 	The new TrainCar to set cursor's TrainCarNode to
	 * 
	 * <dt><b>Preconditions:</b><dd>
	 * 	car must have been initialized
	 * 
	 * <dt><b>Postconditions:</b><dd>
	 * 	cursor reference node will now have car as its TrainCar
	 */
	public void setCursorData(TrainCar car) {
		cursor.setCar(car);
	}
	
	/**
	 * Moves the cursor forward one node if another one exists
	 * 
	 * <dt><b>Preconditions:</b><dd>
	 * 	cursor cannot reference the same node as tail
	 * 
	 * @return
	 * 	Returns true if cursor was successfully moved forward
	 * 
	 * <dt><b>Postconditions:</b><dd>
	 * 	cursor will reference the next node
	 */
	public boolean cursorForward() {
		if(cursor!=null && cursor!=tail) {
			cursor = cursor.getNext();
			return true;
		}
		return false;
	}
	
	/**
	 * Moves the cursor backward one node if another one exists
	 * 
	 * <dt><b>Preconditions:</b><dd>
	 * 	cursor cannot reference the same node as head
	 * 
	 * @return
	 * 	Returns true if cursor was successfully moved backward
	 * 
	 * <dt><b>Postconditions:</b><dd>
	 * 	cursor will reference the previous node
	 */
	public boolean cursorBackward() {
		if(cursor!=null && cursor!=head) {
			cursor = cursor.getPrev();
			return true;
		}
		return false;
	}
	
	/**
	 * Inserts a new TrainCarNode directly after cursor
	 * 
	 * @param newCar
	 * 	The new TrainCar object that the new TrainCarNode will contain
	 * 
	 * <dt><b>Preconditions:</b><dd>
	 * 	newCar must have been initialized
	 * 
	 * <dt><b>Postconditions:</b><dd>
	 * 	cursor will now reference the new TrainCarNode and all of the attributes will be increased
	 * 
	 * @throws IllegalArgumentException
	 * 	Indicates that newCar is null
	 */
	public void insertAfterCursor(TrainCar newCar) throws IllegalArgumentException {
		if(newCar==null) {
			throw new IllegalArgumentException();
		}
		
		boolean switchTail = false;
		
		TrainCarNode newNode = new TrainCarNode(newCar);
		
		if(cursor==tail) {
			switchTail = true;
		}
		
		if(head==null) {
			head = newNode;
			tail = newNode;
			cursor = newNode;
		}
		else if(cursor==tail) {
			cursor.setNext(newNode);
			newNode.setPrev(cursor);
		}
		else {
			newNode.setPrev(cursor);
			newNode.setNext(cursor.getNext());
			cursor.getNext().setPrev(newNode);
			cursor.setNext(newNode);
		}
		
		length += newCar.getCarLength();
		weight += newCar.getCarWeight();
		size++;

		if(switchTail) {
			tail = newNode;
		}
		
		cursor = newNode;
	}
	
	/**
	 * Removes the TrainCarNode directly at cursor
	 * 
	 * <dt><b>Preconditions:</b><dd>
	 * 	There must be at least one TrainCarNode in this TrainLinkedList
	 * 
	 * @return
	 * 	Returns the TrainCar that was removed
	 * 
	 * <dt><b>Postconditions:</b><dd>
	 * 	cursor will now reference the previous TrainCarNode and all of the attributes will be decreased
	 */
	public TrainCar removeCursor() {
		if(cursor==null) {
			return null;
		}
		
		TrainCar removedCar = cursor.getCar();
		
		if(cursor!=head) {
			cursor.getPrev().setNext(cursor.getNext());
		}
		if(cursor!=tail) {
			cursor.getNext().setPrev(cursor.getPrev());
		}
		
		length -= cursor.getCar().getCarLength();
		weight -= cursor.getCar().getCarWeight();
		size--;
		
		if(!removedCar.isEmpty()) {
			value -= removedCar.getProductLoad().getValue();
			weight -= removedCar.getProductLoad().getWeight();
			
			if(removedCar.getProductLoad().isDangerous()) {
				numDangerous--;
			}
		}
		
		if(cursor==head) {
			head = cursor.getNext();
			cursor = head;
		}
		else if(cursor==tail) {
			tail = cursor.getPrev();
			cursor = tail;
		}
		else {
			cursor = cursor.getPrev();
		}
		
		return removedCar;
	}
	
	/**
	 * Finds all the ProductLoad information for a specific product
	 * 
	 * @param name
	 * 	The name of the ProductLoad
	 * 
	 * <dt><b>Postconditions:</b><dd>
	 * 	A formatted table containing the combined information for all products under that name will be printed
	 */
	public void findProduct(String name) {
		double sumWeight = 0, sumValue = 0;
		int numCars = 0;
		boolean checkIsDangerous = false;
		TrainCarNode nodeTemp = head;
		ProductLoad loadTemp;
		
		for(int i=0; i<size; i++) {
			if(!nodeTemp.getCar().isEmpty()) {
				loadTemp = nodeTemp.getCar().getProductLoad();
				
				if(loadTemp.getName().equals(name)) {
					sumWeight += loadTemp.getWeight();
					sumValue += loadTemp.getValue();
					numCars++;
					
					if(loadTemp.isDangerous()) {
						checkIsDangerous = true;
					}
				}
			}
			
			nodeTemp = nodeTemp.getNext();
		}
		
		if(numCars>0) {
			TrainCar carTemp = new TrainCar(0,0);
			carTemp.setProductLoad(new ProductLoad(name,sumWeight,sumValue,checkIsDangerous));
			carTemp.printCar();
		}
		else {
			System.out.println("\nNo record of " + name + " on board train.");
		}
		
	}
	
	/**
	 * Prints the entire TrainLinkedList in a formatted table
	 * 
	 * <dt><b>Postconditions:</b><dd>
	 * 	A formatted table will be printed and the cursor will be represented by an arrow
	 */
	public void printManifest() {
		System.out.println("    CAR:                               LOAD:");
		System.out.println("      Num   Length (m)    Weight (t)  |    Name      Weight (t)     Value ($)   Dangerous");
		System.out.println("    ==================================+===================================================");
		
		TrainCarNode temp = head;
		
		for(int i=1; i<size+1; i++) {
			System.out.println((temp==cursor ? " ->" : "   ") + "    " + i + temp.toString());
			temp = temp.getNext();
		}
	}
	
	/**
	 * Scans through this TrainLinkedList and removes all dangerous TrainCarNodes
	 * 
	 * <dt><b>Postconditions:</b><dd>
	 * 	All dangerous cars will have been removed
	 */
	public void removeDangerousCars() {
		TrainCarNode temp = cursor;
		cursor = head;
		
		while(cursor!=null) {
			if(!cursor.getCar().isEmpty() && cursor.getCar().getProductLoad().isDangerous()) {
				this.removeCursor();
			}
			cursor = cursor.getNext();
		}
		
		// If the car that was selected still exists, set cursor back to it
		if(temp.getCar().isEmpty() || !temp.getCar().getProductLoad().isDangerous()) {
			cursor = temp;
		}
		else {
			cursor = head;
		}
	}
	
	/**
	 * Updates the attributes for this TrainLinkedList when a ProductLoad is specified
	 * 
	 * @param load
	 * 	The ProductLoad to add the values from
	 * 
	 * <dt><b>Postconditions:</b><dd>
	 * 	All attributes will be up to date
	 */
	public void addLoadValues(ProductLoad load) {
		value += load.getValue();
		weight += load.getWeight();
		
		if(load.isDangerous()) {
			numDangerous++;
		}
	}
	
	/**
	 * Returns a String representing the entire TrainLinkedList on a single line
	 * 
	 * @return
	 * 	Returns a String representing the train
	 */
	public String toString() {
		return String.format("Train: %d cars, %,.1f meters, %,.1f tons, $%,.2f value, %s", size,length,weight,value,(numDangerous==0 ? "not dangerous." : "DANGEROUS."));
	}
}
