/**
* The <code>TrainCar</code> class is referenced by TrainCarNode and contains details about each car
*    
* @author Nathan Wood
*    e-mail: nathan.wood@stonybrook.edu
*    Stony Brook ID: 109876625
* <dt><b>Assignment:</b><dd>
*    Homework #2 for CSE 214, Fall 2015
* <dt><b>Date:</b><dd>
*    September 21st, 2015
**/

public class TrainCar {
	// ATTRIBUTES
	
	private double carWeight, carLength;
	private ProductLoad load = null;
	
	// CONSTRUCTORS
	
	/**
	 * Default constructor for a TrainCar object
	 * 
	 * @param carWeight
	 * 	The weight of the car in tons
	 * 
	 * @param carLength
	 * 	The length of the car in meters
	 * 
	 * <dt><b>Postcondition:</b><dd>
	 * 	This TrainCar will have been initialized
	 */
	public TrainCar(double carWeight, double carLength) {
		this.carWeight = carWeight;
		this.carLength = carLength;
	}
	
	// ACCESSORS
	
	/**
	 * Returns the weight of this TrainCar
	 * 
	 * @return
	 * 	The weight of this TrainCar
	 */
	public double getCarWeight() {
		return carWeight;
	}
	
	/**
	 * Returns the length of this TrainCar
	 * 
	 * @return
	 * 	The length of this TrainCar
	 */
	public double getCarLength() {
		return carLength;
	}
	
	/**
	 * Returns the ProductLoad attributed to this TrainCar
	 * 
	 * @return
	 * 	The ProductLoad of this TrainCar
	 */
	public ProductLoad getProductLoad() {
		return load;
	}
	
	// MUTATORS
	
	/**
	 * Sets the ProductLoad of this TrainCar
	 * 
	 * @param load
	 * 	The new ProductLoad to be attributed to this TrainCar
	 * 
	 * <dt><b>Preconditions:</b><dd>
	 * 	load must have been initialized
	 */
	public void setProductLoad(ProductLoad load) {
		this.load = load;
	}
	
	// METHODS
	
	/**
	 * Returns whether the TrainCar is empty or not
	 * 
	 * @return
	 * 	True/False whether a ProductLoad exists or not
	 */
	public boolean isEmpty() {
		return (load==null);
	}
	
	/**
	 * Prints a header and formatted information about this TrainCar
	 * 
	 * <dt><b>Postconditions:</b><dd>
	 * 	A formatted table containing this TrainCar and ProductLoad information has been printed out
	 */
	public void printCar() {		
		System.out.println("        Name      Weight (t)     Value ($)   Dangerous");
		System.out.println("    ===================================================");
		
		if(load!=null) {			
			System.out.printf("%14s%,14.1f%,14.2f%12s\n",load.getName(),load.getWeight(),load.getValue(),(load.isDangerous() ? "YES" : "NO"));
		}
		else {
			System.out.printf("%14s%,14.1f%,14.2f%12s\n","Empty",carWeight,0.0,"NO");
		}
	}
}
