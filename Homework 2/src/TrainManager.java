/**
* The <code>TrainManager</code> class is a test class for TrainLinkedList and its subclasses
*    
* @author Nathan Wood
*    e-mail: nathan.wood@stonybrook.edu
*    Stony Brook ID: 109876625
* <dt><b>Assignment:</b><dd>
*    Homework #2 for CSE 214, Fall 2015
* <dt><b>Date:</b><dd>
*    September 21st, 2015
**/

import java.util.Scanner;
import java.util.InputMismatchException;

public class TrainManager {
	public static void main(String args[] ) {
		// Keyboard input variable
		Scanner input = new Scanner(System.in);
		
		// Linked list variables
		TrainLinkedList train = new TrainLinkedList();
		
		// Menu variables
		String selection = "";
		boolean terminate = false;
		
		// Terminate is only true when the user selects (Q) Quit
		while(!terminate) {
			System.out.println("\n(F) Cursor Forward\n(B) Cursor Backward\n(I) Insert Car After Cursor\n(R) Remove Car At Cursor\n(L) Set Product Load"
					+ "\n(S) Search For Product\n(T) Display Train\n(M) Display Manifest\n(D) Remove Dangerous Cars\n(Q) Quit");
			
			// Breaking from this loop returns to the main menu
			while(!terminate) {
				// Retrieve the user's menu selection
				System.out.print("\nEnter a selection: ");
				selection = input.nextLine().toUpperCase();
				
				System.out.println(); // Adding spacing
				
				// (F) CURSOR FORWARD
				if(selection.equals("F")) {
					// Moves the cursor forward if a next node exists
					if(train.cursorForward()) {
						System.out.println("Cursor moved forward.");
					}
					else {
						System.out.println("No next car, cannot move cursor forward.");
					}
					
					break;
				}
				
				// (B) CURSOR BACKWARD
				else if(selection.equals("B")) {
					// Moves the cursor backward if a previous node exists
					if(train.cursorBackward()) {
						System.out.println("Cursor moved backward.");
					}
					else {
						System.out.println("No previous car, cannot move cursor backward.");
					}
					
					break;
				}
				
				// (I) INSERT CAR AFTER CURSOR
				else if(selection.equals("I")) {
					try {
						// Retrieve user input to create a TrainCar object
						System.out.print("Enter car length in meters: ");
						double newCarLength = input.nextDouble();
						
						System.out.print("Enter car weight in tons: ");
						double newCarWeight = input.nextDouble();
						
						// Insert the new TrainCar after the cursor
						train.insertAfterCursor(new TrainCar(newCarWeight,newCarLength));
						
						System.out.printf("\nNew train car %.1f meters and %.1f tons inserted into train.\n",newCarLength,newCarWeight);
					} catch(InputMismatchException e) {
						System.out.println("\nError: the user input did not match the required data type.");
					}
					
					// Clear user input
					input.nextLine();
					break;
				}
				
				// (R) REMOVE CAR AT CURSOR
				else if(selection.equals("R")) {
					// Attempt to remove the TrainCar under the cursor and store it in temp
					TrainCar temp = train.removeCursor();
					
					if(temp==null) {
						System.out.println("There is no train car to remove.");
					}
					else {
						System.out.println("Car successfully unlinked. The following load has been removed from the train:\n");
						temp.printCar();
					}
					
					break;
				}
				
				// (L) SET PRODUCT LOAD
				else if(selection.equals("L")) {
					// Make sure the train has a car first
					if(train.getCursorData()==null) {
						System.out.println("Please add a car to the train before adding a product load.");
					}
					else {
						try {
							// Retrieve user input to construct a ProductLoad object
							System.out.print("Enter product name: ");
							String newName = input.nextLine();
							
							System.out.print("Enter product weight in tons: ");
							double newWeight = input.nextDouble();
							
							System.out.print("Enter product value in dollars: ");
							double newValue = input.nextDouble();
							
							// Clear user input
							input.nextLine();
							
							System.out.print("Enter is product dangerous? (y/n): ");
							String isDangerousInput = input.nextLine().toUpperCase();
							
							boolean newIsDangerous = false;
							
							if(isDangerousInput.equals("Y")) {
								newIsDangerous = true;
							}
							else if(isDangerousInput.equals("N")) {
								newIsDangerous = false;
							}
							else {
								throw new InputMismatchException();
							}
							
							// Create the new ProductLoad to insert into the cursor's position
							ProductLoad tempLoad = new ProductLoad(newName,newWeight,newValue,newIsDangerous);
							
							train.getCursorData().setProductLoad(tempLoad);
							train.addLoadValues(tempLoad);
							
							System.out.printf("\n%.1f tons of %s added to the current car.\n",newWeight,newName);
						} catch(InputMismatchException e) {
							System.out.println("\nError: the user input did not match the required data type.");
						}
					}
					
					break;
				}
				
				// (S) SEARCH FOR PRODUCT
				else if(selection.equals("S")) {
					try {
						// Retrieve user input for the ProductLoad name to search for
						System.out.print("Enter product name: ");
						String name = input.nextLine();
						
						train.findProduct(name);
					} catch(InputMismatchException e) {
						System.out.println("\nError: the user input did not match the required data type.");
					}
					
					break;
				}
				
				// (T) DISPLAY TRAIN
				else if(selection.equals("T")) {
					// Print the single-line String represents the train as a whole
					System.out.println(train.toString());
					
					break;
				}
				
				// (M) DISPLAY MANIFEST
				else if(selection.equals("M")) {
					// Prints each car separately with the header
					train.printManifest();
					
					break;
				}
				
				// (D) REMOVE DANGEROUS CARS
				else if(selection.equals("D")) {
					// Remove all cars that have isDangerous = true
					train.removeDangerousCars();
					System.out.println("Dangerous cars successfully removed from the train.");
					
					break;
				}
				
				// (Q) QUIT
				else if(selection.equals("Q")) {
					// Breaks the while loops, exiting the menu
					terminate = true;
					break;
				}
			}
		}
		
		// This point is only reached after both while loops are broken
		System.out.println("Program terminating successfully...");
		input.close();
	}
}
